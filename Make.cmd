SET npm_config_arch=ia32
set npm_config_disturl=https://atom.io/download/electron
SET npm_config_msvs_version=2015
SET npm_config_target=1.7.9
node scripts/updateVersion.js

CALL .\node_modules\.bin\electron-rebuild.cmd -a ia32
CALL yarn build
CALL yarn release -- --platform win --arch ia32
powershell -noexit "& 'scripts\upload.ps1 ' -gettedServerName 'MY-PC'"