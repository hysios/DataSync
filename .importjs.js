module.exports = {
    excludes: [
        'dist',
        'build',
        'scripts'
    ],
    environments: ['node']
    // continue with the rest of your settings...
}