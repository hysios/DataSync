RELEASE_ENVS=npm_config_arch=x64 npm_config_disturl=https://atom.io/download/electron npm_config_msvs_version=2015 npm_config_target=1.7.9
DIST_FILENAME:=`cat dist/build.yml | grep path: | sed  's/path: //'`

.PHONY: build

run: 
	yarn run dev

build:
	@yarn build

# release (废弃) 可以部署二进制包文件到 https://github.com/wanliu/DataSync/releases 必须要 GH_TOKEN 环境变量设置
# 现在是部署到 s3 jiejie-deploy bucket 中去，需要 aws key, secret
publish: build
	@node scripts/updateVersion.js
	@$(RELEASE_ENVS) yarn release -- --platform win
	@scripts/upload.sh
	@echo https://s3.cn-north-1.amazonaws.com.cn/jiejie-deploy/$(DIST_FILENAME)

