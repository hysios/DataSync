const semver = require('semver');
const yaml = require('js-yaml');
const path = require('path');
const fs   = require('fs');
const packageJson = path.join(process.cwd(), 'package.json');

try {
	let pack = require(packageJson);
	// let doc = yaml.safeLoad(fs.readFileSync('./dist/latest-mac.yml', 'utf8'));
	let newVersion = semver.inc(pack.version, 'prerelease', 'build');
	console.log(newVersion);
	pack.version = newVersion;
    fs.writeFileSync(packageJson, JSON.stringify(pack, null, 2));
} catch(e) {
	console.log(e)
}


