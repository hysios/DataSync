$FILE = $(Get-Content .\dist\build.yml | findstr 'path:' | % { $_ -replace "path: ","" })
$BUILDFILE = "dist/$FILE"
$BUCKET_NAME = "jiejie-deploy"
$BUCKET = "s3://$BUCKET_NAME"
$CONFIG = "build.yml"
$VERSIONFILE= "dist/$CONFIG"

aws s3 cp "$BUILDFILE" "$BUCKET"
aws s3 cp "$VERSIONFILE" "$BUCKET"
aws s3api put-object-acl --bucket $BUCKET_NAME --key "$FILE" --grant-read uri=http://acs.amazonaws.com/groups/global/AllUsers
aws s3api put-object-acl --bucket $BUCKET_NAME --key $CONFIG --grant-read uri=http://acs.amazonaws.com/groups/global/AllUsers
echo https://s3.cn-north-1.amazonaws.com.cn/jiejie-deploy/$FILE