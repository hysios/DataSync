import React, { Component } from 'react';
import './App.css';
import logo from './mini_logo.png';

import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

import {
  Home, 
  Product, 
  Purchase, 
  PurchaseDetail, 
  Sale,
  SaleDetail,
  Inventory,
  Settings,
  Printer,
  Upgrade
} from './components';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <div className="navbar">
            <div className="container">
              <div className="App-toolbar" >
                <ul className="commander">
                  <li className="btn-grad"><Link to="/">首页</Link></li>
                  <li className="btn-grad"><Link to="/product">货品</Link></li>
                  <li className="btn-grad"><Link to="/purchase">采购</Link></li>
                  <li className="btn-grad"><Link to="/sale">销售</Link></li>
                  <li className="btn-grad"><Link to="/inventory">库存</Link></li>
                  <li className="btn-grad"><Link to="/settings">设置</Link></li>
                  <li className="btn-grad"><Link to="/printer">打印</Link></li>
                  <li className="btn-grad"><Link to="/upgrade">更新</Link></li>
                </ul>
              </div>
            </div>
          </div>
          <div className="App">
            <div className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
              <h2 className="App-Brand">数据同步器</h2>
            </div>
            <Route exact path="/" component={Home}/>
            <Route exact path="/product" component={Product}/>            
            <Route exact path="/purchase" component={Purchase}/>
            <Route exact path="/purchase/:code" component={PurchaseDetail}/>
            <Route exact path="/sale" component={Sale}/>
            <Route exact path="/sale/:code" component={SaleDetail}/>
            <Route exact path="/inventory" component={Inventory}/>
            <Route exact path="/settings" component={Settings}/>
            <Route exact path="/printer" component={Printer}/>
            <Route exact path="/upgrade" component={Upgrade}/>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
