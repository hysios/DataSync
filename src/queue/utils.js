const debug = require('debug')('command-queue:utils');

function getAttr(msg, name) {
	if (msg.MessageAttributes) {
		let attr = msg.MessageAttributes[name];
		if (!attr) {
			debug('not getAttr name', name)
			return null
		}
		
		if (attr.DataType == 'String') {
			return attr.StringValue;
		} else if (attr.DataType == 'Number') {
			return parseInt(attr.StringValue);
		} else {
			return attr.StringValue;
		}
	} else {
		return null
	}
}


module.exports = {
	getAttr
}