const EventEmitter = require('events');
const uuid = require('uuid/v4');
const {BUF_MAXSIZE} = require('./constants');
const debug = require('debug')('command-queue:command');
const whileLoop = require('promise-while-loop');
const Promise = require('bluebird');
const {getAttr} = require('./utils');
const {
	createQueue,
	sendMessage,
	sendMessageBuf,
	sendMessagePart,
	receiveMessage,
	deleteMessage,
	deleteQueue,
	messageLoop
} = require('../sqs');

class Command extends EventEmitter {
	constructor(cmdName, options = {namespace: 'data-sync'}) {
		super()

		this.namespace = options['namespace'];
		this.cmdName = cmdName;
		this.affix = options['fifo'] ? '.fifo' : '';
		this.queueName = `command-queue-${this.namespace}${this.affix}`;
		debug('create Command arguments', options);
		// this.queueURL();
		this.commandOptions = options['commandOptions'];
		this.bufSize = BUF_MAXSIZE;
		this.begin = this.begin.bind(this);
		this.queueURL = this.queueURL.bind(this);
		this.createReturnQueue = this.createReturnQueue.bind(this);
		this.splitMsg = this.splitMsg.bind(this);
		this.listenReturn = this.listenReturn.bind(this);
		this.closeQueue = this.closeQueue.bind(this);
		this.index = 0;
		this.bodies = {};
		this.messageHandles = [];
	}

	queueURL() {
		if (this._queueURL) {
			return Promise.resolve(this._queueURL);
		} else {
			return new Promise((resolve) => {
				debug('queueName', this.queueName)
				createQueue(this.queueName).then((url) => {
					debug('get queueURL', url);
					this._queueURL = url;
					resolve(url);
				});
			});
		}
	}

	reset() {
		this.index = 0;
		this.bodies = {};
		this.messageSum = 0;
		this.messageHandles = [];
	}

	send(data) {
		this.reset();
		debug('send', data)
		return this.begin(data)
			.then(this.createReturnQueue)
			.then(this.splitMsg)
			.then(this.listenReturn)
			.then(this.closeQueue)
			.then(() => {
				return {
					data: this.getData(),
					sum: this.messageSum
				};
			})
	}

	begin(data) {
		let cmdId = uuid(),
			buf = JSON.stringify(data),
			count = Math.ceil(buf.length / this.bufSize);
		let msg = {
			id: cmdId, 
			type: 'COMMAND',
			command: this.cmdName,
			body: data,
			count: count
		};
		debug('build msg', msg);
		return Promise.resolve(msg);
	}

	createReturnQueue(msg) {
		return new Promise((resolve, reject) => {
			const queueReturnName = `command-return-${msg.id}${this.affix}`;
			debug('createReturnQueue', queueReturnName)
			createQueue(queueReturnName)
				.then(url => {
					debug('returnUrl:', url);
					msg.returnURL = url;
					this.returnURL = url;
					resolve(msg);
				})
				.catch(reject)
		})
	}

	splitMsg(msg) {
		let data = msg.body;
		delete msg['body'];
		let buf = JSON.stringify(data);
		let {count} = msg;

		return new Promise((resolve, reject) => {
			let parts = [];
			this.queueURL().then(url => {
				debug('getQueueURL', url);

				for (let i = 0; i < count; i++) {
					let part = buf.slice(i*this.bufSize, (i+1) * this.bufSize);
					debug('sendMessagePart url:', url);
					debug('with msg:', msg);
					debug(`at ${i} of ${count}`);
					// debug('split data', part);
					// parts.push(part);
					parts.push(sendMessagePart(url, msg, i, count, part));
				}

				Promise
					.all(parts)
					.then((values) => {
						debug('send all message success');
						msg.values = values
						resolve(msg)
					})
					.catch(reject)		
			})

		})
	}

	listenReturn(msg) {
		debug('listen msg', msg);
		return Promise.try(() => {
			return whileLoop(data => {
				let checked = data.Messages && data.Messages.length > 0;
				debug('whileLoop check', checked)
				const messages = (data.Messages || []);
				for (let i = 0; i < messages.length; i++) {
					let message = messages[i];
					try {
						debug('listenReturn receive message')
						debug('raw messageAttributes', message.MessageAttributes);	
						let	part = getAttr(message, 'MsgPart'),
							partCount = getAttr(message, 'MsgPartCount');
						debug('msg part %d of %d', part, partCount);

						if (part || partCount) {
							if (this.index == 0) {
								debug('receive begin')
								this.emit('begin');
							}

							if (this.index++ < partCount) {
								debug('receive return data part:', part);
								debug('part %d type %s', part, typeof part);

								this.messageHandles.push({
									Id: message.MessageId,
									ReceiptHandle: message.ReceiptHandle
								});
								this.bodies[part] = message.Body;
								// this.emit('data', message.Body);
								// deleteMessage(msg.returnURL, message)
							}

							if (this.index == partCount ) {
								this.messageSum = getAttr(message, 'BodyALLSum');
								this.emit('end');
								debug('receive return data complete')
								return false
								// await deleteMessage(msg.returnURL);
							}
						}	
					} catch (err) {
						console.error(err);
						return false;
					}
				}				
				return true;
			}, (data) => {
				debug('receive', msg.returnURL);
				return receiveMessage(msg.returnURL);
			});
		});
	}

	getData() {
		debug('getData %O', Object.keys(this.bodies));
		let idxs = Object.keys(this.bodies).sort();
		return idxs.map(i => this.bodies[i]).join('');
	}

	closeQueue(msg) {
		return deleteQueue(this.returnURL)
	}
}

module.exports = Command;