const EventEmitter = require('events');
const {
	sendMessagePart,
	sendMessageBatch,
	deleteMessage,
	deleteMessageBatch
} = require('../sqs');
const md5 = require('md5');
const {BUF_MAXSIZE} = require('./constants');
const {getAttr} = require('./utils');
const debug = require('debug')('command-queue:message');
const Promise = require('bluebird');
const whileLoop = require('promise-while-loop');


class Message extends EventEmitter {

	static from(message, options={}) {
		const id = getAttr(message, 'MsgID'),
			type = getAttr(message, 'Type'),
			command = getAttr(message, 'Command'),
			index = getAttr(message, 'MsgPart'),
			count = getAttr(message, 'MsgPartCount'),
			returnURL = getAttr(message, 'ReturnURL'),
			queueURL = options['queueURL'];
		// if (type !== 'RETURN') {
		// 	throw Error('invalid message type, can\'not be RETURN message');
		// }
		if (index !== 0) {
			throw Error('must first part message');
		}
		return new Message(id, {type, command, count, returnURL, queueURL})
	}

	constructor(id, options = {count: 1}) {
		super();
		this.msgID = id;
		this.command = options['command'];
		this.count = options['count'];
		this.queueURL = options['queueURL'];
		this.returnURL = options['returnURL'];
		this.receIndex = 0;
		this.buf = "";
		this.bodies = {};
		this.messageHandles = [];
	}

	/**
	 * push 消息是把分包的消息，进行合并的过程，当分包全部接受后，会返回 true 值，否则返回 false
	 * 当合并完成后，通常会触发 command:end 命令接收事件。
	 * @param  {Message} message [description]
	 * @return {Boolean}         [description]
	 */
	push(message) {
		if (getAttr(message, 'MsgID') !== this.msgID) {
			throw Error('incorrect message id ');
		}

		if (this.receIndex++ < this.count) {
			let index = getAttr(message, 'MsgPart')
			this.bodies[index] = message.Body;
			// this.buf += message.Body;
			this.messageHandles.push({
				Id: message.MessageId,
				ReceiptHandle: message.ReceiptHandle
			});
			return this.receIndex == this.count;
		} else {
			throw Error("buffer is full");
		}

		return false
	}

	close() {
		debug('close message');
		debug('deleteMessages %s of %O', this.queueURL, this.messageHandles.map(msg=> msg.Id));
		return deleteMessageBatch(this.queueURL, this.messageHandles);
	}

	closeReturn() {
		debug('close return message');
		debug('deleteMessages', this.messageHandles.map(msg=> msg.Id));
		// return deleteMessageBatch(this.queueUrl, this.messageHandles);		
	}

	getData() {
		let idxs = Object.keys(this.bodies).sort();
		return idxs.map(i => this.bodies[i]).join('');
	}

	/**
	 * reply 回复消息，自动分割大于列队传输尺寸的大小，列队传输数据不能大于 256KB 所以我们必须分批逐步发送小于 256KB
	 * 数据到对方，然后再组合成一个完整的数据，进行处理 queue-command 就是自动化这个过程，无须你为接受与发送分包数据
	 * 的繁琐工作。
	 * @param  {[type]} data [description]
	 * @return {[type]}      [description]
	 */
	reply(data) {

		return new Promise((resolve, reject) => {
			let buf = JSON.stringify(data),
				count = buf.length / BUF_MAXSIZE,
				msg = {
					id: this.msgID,
					type: 'RETURN',
					command: this.command,
					bodyAllSum: md5(buf)
				},
				parts = [];
			
			debug('reply url %s', this.returnURL) 

			for (let i = 0; i < count; i++) {
				let part = buf.slice(i*BUF_MAXSIZE, (i+1) * BUF_MAXSIZE);
				// parts.push(sendMessagePart(this.returnURL, msg, i, count, part));
				parts.push(part)
			}

			this.sendAllMessage(this.returnURL, msg, parts).then(( ) => {
				resolve(msg);
			})
			.catch((err) => {
				// this.close();
				reject(err);
			});
		})
	}

	sendAllMessage(url, msg, parts) {
		return Promise.try(() => {
			let index = 0, count = parts.length;

			return whileLoop(data => {				
				return index < count;
			}, (data) => {
				let part = parts[index++];
				debug('sendMessagePart part %d of %d', index, count);
				return sendMessagePart(this.returnURL, msg, index, count, part)

				// return receiveMessage(msg.returnURL);
			});
		});		
	}
}

module.exports = Message;