const Command = require('../command');
const md5 = require('md5');
const getProducts = new Command('products', {
	method: 'POST',
	path: '/products',
	namespace: 'test'
});

getProducts
	.send({offset: 0, limit: 100})
	.then((result) => {
		console.log('result', result);
	})
	.catch(err => {
		console.error(err);
	})
