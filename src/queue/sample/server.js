const getManager = require('../commandManager');
const randomstring = require('randomstring');
const manager = getManager('test');


manager.on('command:products', (msg) => {
	console.log('command:products', msg)
	let products = [];

	for (let i = 0;i < 400; i++) {
		products.push(randomstring.generate(4000));
	}

	msg.reply(products);
})


manager.listen();