const Command = require('../command');
const md5 = require('md5');
const getProducts = new Command('products', {
	method: 'POST',
	path: '/products',
	namespace: 'test'
});

// getProducts
// 	.send({offset: 0, limit: 100})
// 	.then((result) => {
// 		console.log('result', result);
// 	})
// 	.catch(err => {
// 		console.error(err);
// 	})

// getProducts.on('begin', () => {

// });

// getProducts.on('data', (data) => {
// 	// console.log(data)
// });

// getProducts.on('end', (raw, sum) => {
// 	try {
// 		data = JSON.parse(raw);
// 		console.log(data.length, sum, md5(raw) === sum);
// 		console.log('end length', raw.length);
// 	} catch(err) {
// 		console.error(err, err.stack);
// 	}
// 	// process.exit(0);
// });

(async () => {
	for (let i = 0; i < 100; i++) {
		try {
			let result = await getProducts.send({offset: 0, limit: 100});
			let {data, sum } = result;
			console.log(data.length, sum, md5(data) === sum);

			if (md5(data) !== sum) {
				console.log('failed at', i);
				return 
			}
		} catch (err) {
			console.log(err)
		}
	}
})();