const EventEmitter = require('events');
const Command = require('./command');
const Message = require('./message');
const {BUF_MAXSIZE} = require('./constants');
const {getAttr} = require('./utils');
const debug = require('debug')('command-queue:manager');

const {
	createQueue,
	sendMessage,
	sendMessageBuf,
	sendMessagePart,
	receiveMessage,
	deleteMessage,
	messageLoop
} = require('../sqs');


const namespaces = {};

class CommandManager extends EventEmitter {
	constructor(options  = {namespace: 'data-sync'}) {
		super();
		this.namespace = options['namespace'];
		this.affix = options['fifo'] ? '.fifo' : '';
		this.queueName = `command-queue-${this.namespace}${this.affix}`;
		this.bufSize = BUF_MAXSIZE;
		this.msgIds = {};
	}

	create(command, options = {}) {
		return new Command(command, {
			commandOptions: options, 
			namespace: this.namespace
		});
	}

	queueURL() {
		if (this._queueURL) {
			return Promise.resolve(this._queueURL);
		} else {
			return new Promise((resolve) => {
				debug('create queueName', this.queueName)
				createQueue(this.queueName).then((url) => {
					debug('return queue url', url)
					this._queueURL = url
					resolve(url);
				});
			});
		}
	}

	/**
	 * listen 监听命令消息主轮询，当收到一个完整的消息包时，会抛出事件 command:xxxxx 以 command：为前
	 * 缀，可以在这个事件响应方法中处理命令的功能，并调用 Message.reply 进行回应。注意：必须调用 reply 
	 * 进行回应，因为每个命令创建的 return 列队必须在 reply 后删除，你可以返回任意值 ，如: 成功时，返回
	 * true 或，失败时，反回 false 让调用方接受到状态，CommandManager 会在 Message.reply 后，回收
	 * 它的资源以必免造成垃圾资源的堆积。
	 */
	async listen() {
		let queueurl = await this.queueURL();

		while(1) {
			debug('listen on queue url:', queueurl);
			let data = await receiveMessage(queueurl);

			if (data.Messages && data.Messages.length) {
				debug(`receive ${data.Messages.length} messages`);
			}

			(data.Messages || []).forEach((message) => {
				let msgId = getAttr(message, 'MsgID'),
					type = getAttr(message, 'Type'),
					command = getAttr(message, 'Command'),
					msg = this._referMessage(message, {queueURL: queueurl});				
				
				try {
					debug('raw messageAttributes', message.MessageAttributes);
					debug(`refer message with id: ${msgId} type: ${type} command: ${command}`)

					if (msg.push(message)) {
						debug('receive command:', command);
						debug('with full msg length:', msg.getData().length)
						this.emit('command:' + command, msg, message);
						this._closeMessage(msg);
							// await this._closeMessage(message);
					}

				} catch (err) {
					this._closeMessage(msg);
					console.log(err)
				}
			})
		}
	}

	_referMessage(msg, options = {}) {
		let msgId = getAttr(msg, 'MsgID'),
			count = getAttr(msg, 'MsgPartCount'),
			message = this.msgIds[msgId];

		if (!message) {
			message = Message.from(msg, options);
			this.msgIds[msgId] = message;
		}

		return message;
	}

	_closeMessage(msg) {
		if (msg.msgID) {
			delete this.msgIds[msg.msgID]
			msg.close()
		}
	}
}

module.exports = function getManager(namespace = 'data-sync') {
	let manager = namespaces[namespace];
	if (!manager) {
		manager = new CommandManager({namespace: namespace})
		namespaces[namespace] = manager;
	}

	return manager;
}