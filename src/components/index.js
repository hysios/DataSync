const path = require('path');

function requireAll(r) {
	r.keys().filter(ignoreIndex).forEach((m) => {
		exportModule(m, r);
	}); 
}

function ignoreIndex(m) {
	return !/index.js$/.test(m);
}

function exportModule(m, r) {
	let name = path.basename(m, '.js')
	module.exports[name] = r(m).default;
}

requireAll(require.context(__dirname, true, /\.js$/));