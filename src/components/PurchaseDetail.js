import React from 'react';
import DataTabler from './DataTabler';
import Table from './Table';
import moment from 'moment';
import Currency from './Currency';
import Number from './Number';

import NotificationSystem from 'react-notification-system';
import {API_SERVER} from '../constants';


export default class PurchaseDetail extends DataTabler {
	state = {
		received: {client: {}, store: {}}
	}

	fetchSource(options = {}) {
		let {params} = this.props.match,
			headers = {
			   	'Accept': 'application/json, text/plain, */*',
			    'Content-Type': 'application/json'
			};

		return fetch(`${API_SERVER}/purchase/${params.code}`, {
		    headers,
			method: 'POST', 
			body: JSON.stringify(options)
		}).then(response => {
			return response.json()
		})
		.then(this.fetchError)
		.then((result) => {
			this.setState({received: result});
    		return result.receivedDetails;
		})
		.catch((err) => {
			this.refs.notificationSystem.addNotification({
				message: err.message,
				level: 'error',
				dismissible: false
			});
		})
	}

	render() {
		const { received, data, pages, loading } = this.state,
			{ columns } = this.props;

		return (<div className="container">
			<ul className="saleHeader">
				<li>客户名称：{received.client.shortname}</li>
				<li>客户电话：{received.phone}</li>
				<li>单号: {received.code}</li>
			</ul>
			<ul className="saleHeader">
				<li>送货地址：{received.shipto}</li>
				<li>出货仓: {received.store.name}</li>
				<li>开单日期: {moment(received.billdate).format("YYYY MMM Do")}</li>
			</ul>
	        <Table
	          	data={data}
	          	columns={columns}
	          	manual
  	          	pages={pages} // Display the total number of pages
          		loading={loading} // Display the loading overlay when we need it
	          	defaultPageSize={10}
				onFetchData={this.fetchData} // Request new data when things change
	          	className="-striped -highlight"
	        />
            <NotificationSystem ref="notificationSystem" position="tc" />
		</div>)
	}
}


PurchaseDetail.defaultProps = {
	columns: [
	{
		Header: "商品编码",
    	accessor: "good.code",
    },	          	
    {
    	Header: "商品名称",
    	accessor: "good.name",
    },
    {
    	Header: "数量",
    	accessor: "quantity",
        style: {textAlign: 'right'},
        Cell: row => (
            <Number value={row.value} />
        )	
    },
    {
    	Header: "进价",
    	accessor: "price",
        style: {textAlign: 'right'},
        Cell: row => (
            <Currency value={row.value} />
        )

    },
    {
    	Header: "总计",
    	accessor: "amount",
        style: {textAlign: 'right'},
        Cell: row => (
            <Currency value={row.value} />
        )
    }
	]

}