import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
moment.locale('zh-cn');

export default class Date extends React.Component {
	render() {
		return <span>{moment(this.props.value).format("YYYY MMM Do")}</span>;
	}
}

Date.propTypes = {
	value: PropTypes.any
};