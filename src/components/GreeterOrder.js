import React from 'react';
import TimeAgo from './TimeAgo';
import classNames from 'classnames';
import FaPrint from 'react-icons/lib/fa/print';
import Engine from '../engine';

export default class GreeterOrder extends React.Component {
	styles = {
		unit: {
			width: 100
		},
		quantity: {
			width: 100
		},
		spec: {
			width: 200
		}
	}

	constructor(props) {
		super(props)

		this.state = {
			order: props.order,
			printer: props.printer,
			printerTitle: '初始化中...',
			status: props.status
		}	
	}

	componentWillReceiveProps(nextProps) {
		let newStatus = {};
		if (nextProps.printer) {
			newStatus = Object.assign(newStatus, {
				printer: nextProps.printer,
				printerTitle: '打印设备: ' + nextProps.printer.name
			})
		}

		if (nextProps.status) {
			newStatus = Object.assign(newStatus, {
				status: nextProps.status
			})
		}

		if (Object.keys(nextProps).length > 0) {
			this.setState(newStatus);
		}
	}

	orderStatus(order) {
		switch (order.status) {
		case 'new':
			return <span>保存中</span>;
		case 'saved':
			return <span>打印中</span>;
		case 'printed':
			return <span></span>;
		default:
			return <span></span>;
		}
	}

	orderTitle(order) {
		switch (order.status) {
		case 'new':
			return '新订单未保存';
		case 'saved':
			return '点击打印开单';
		case 'printed':
			return '已打印';
		default:
			return '';
		}
	}

	handlePrint(order) {
		Engine.send('order:print', order)
		// this.setState({
		// 	status: 'print',
		// })
		// console.log('print', order)
	}

	render() {
		let {order, printerTitle} = this.state
		let orderClass = classNames('relative', 'order-item', order.status)
		return (<div className={orderClass} title={this.orderTitle(order)}>
			<div className="toolbar">	
				<button title={printerTitle} onClick={this.handlePrint.bind(this, order)} >
					<FaPrint />
				</button>
			</div>
			<h4 className="cust-headerH1">订单序号：{order.orderId}</h4>
			<ul className="cust-header">
				<li>客户：{order.customerName}</li>
				<li>客户电话：</li>
				<li>开单时间：<TimeAgo value={order.createdAt} /></li>
				<li>送货地址: {order.address}</li>
				<li>业务员：{order.salerName}</li>
				<li>出货仓：{order.storehouseName}</li>
			</ul>
			{this.renderTable(order)}
			<div className="printProgress">
				<div className="progress-bar">
					<div className="progress-value" style={{width: '100%'}}>
					</div>
				</div>
			</div>
		</div>)
	}

	renderTable(order) {
		let status = this.state.status || order.status;
		let tableClass = classNames('greeter-table', status)

		return (<table className={tableClass}>
			<thead>
				<tr>
					<th>货品</th>
					<th style={this.styles.spec}>规格</th>
					<th style={this.styles.quantity}>数量</th>
					<th style={this.styles.unit}>单位</th>
					<th>备注</th>
				</tr>
			</thead>
			<tbody>
				{order.items.map((item, i) => (
					<tr key={i} >
						<td className="text-left">{item.productName}</td>
						<td className="text-left text-width">{item.spec}</td>
						<td className="text-right">{item.quantity}</td>
						<td className="test-unitName">{item.unitName}</td>
						<td className="test-liping">{item.memo}</td>
					</tr>
				))}

				{order.gifts.map((item, i) => (
					<tr key={i} className="double" >
						<td className="text-left">{item.productName}</td>
						<td className="text-left text-width">{item.spec}</td>
						<td className="text-right">{item.quantity}</td>
						<td className="test-unitName">{item.unitName}</td>
						<td className="test-liping">礼品</td>
					</tr>
				))}				
			</tbody>

		</table>)
	}

}

GreeterOrder.defaultProps = {
	order: React.PropTypes.object
}