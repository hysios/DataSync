import React from 'react';
import 'react-table/react-table.css';
import {
  Link
} from 'react-router-dom'
import Date from './Date';
import Currency from './Currency';
import DataTabler from './DataTabler';
import {API_SERVER} from '../constants';
import FaCheck from 'react-icons/lib/fa/check';
import FaClose from 'react-icons/lib/fa/close';

export default class Sale extends DataTabler {
	fetchSource(options = {}) {
		return fetch(`${API_SERVER}/sales`,{
		    headers: {
			   	'Accept': 'application/json, text/plain, */*',
			    'Content-Type': 'application/json'
			},
			method: 'POST', 
			body: JSON.stringify(options)
		}).then(response => {
			return response.json()
		})
	}
}

const styles = {
    checked: {color: 'green'},
    closed: {color: 'red'}
};

Sale.defaultProps = {
	columns: [
        {
        	Header: "单号",
        	accessor: "code",
        },
        {
        	Header: "下单时间",
        	accessor: "billdate",
            style: {textAlign: 'center'},
        	Cell: row => (
        		<Date value={row.value} />
        	)		            	
        },
        {
        	Header: "客户",
        	accessor: "client.shortname",
        },		            
        {
        	Header: "总金额",
        	accessor: "totalamt",
        	style: {textAlign: 'right'},
        	Cell: row => (
        		<Currency value={row.value} />
        	)			            	
        },
        {
            Header: "开单",
            accessor: "printed",
            style: {textAlign: 'center'},
            Cell: row => (
                row.value ? <FaCheck style={styles.checked} /> : <FaClose style={styles.closed } />
            )
        },
        {
        	Header: "工具",
        	accessor: "code",
            style: {textAlign: 'center'},
        	Cell: row => (
	          <Link className="btn-grad" to={'/sale/' + row.value} >打开</Link>
         	)   
        }
	]
}