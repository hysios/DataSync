import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

export default class Table extends React.Component {
	render () {
		return (<ReactTable
			previousText='上一页'
			nextText='下一页'
			loadingText='加载中...'
			noDataText='没有找到数据'
			pageText='页'
			ofText='之'
			rowsText='行'
          	{...this.props}
      	
        />);		
	}
}