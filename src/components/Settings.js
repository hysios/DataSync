import React, { Component } from 'react';
import { Form, Text, Select } from 'react-form';
import NotificationSystem from 'react-notification-system';

// eslint-disable-next-line
const remote = eval("require('electron').remote");
// eslint-disable-next-line
const {ipcRenderer} = eval("require('electron')");

let config = {
	database: {}
};
if (remote) {
	config = remote.getGlobal('config') || {};
}

// import 'react-form/layout.css';
// "host": "192.168.1.222",
// "username": "hysios",
// "password": "asdfasdf",
// // "dbname": "SD11831N_WeiXin",
// "dbname": "SD11831N_20161101",
// "dialect": "mssql"
export default class Settings extends Component {
	constructor(props) {
		super(props);
		let {database} = config;
		this.state = {database};
	}

	render() {
		return (
			<div>
        	<NotificationSystem ref="notificationSystem" position="tc" />

			<Form
			defaultValues={this.state.database}

			onSubmit={(values) => {
				this.refs.notificationSystem.addNotification({
					message: '保存成功, 配置重启后生效',
					level: 'success',
					dismissible: false
				});
				if (ipcRenderer) {
					console.log(values);
					config.database = values;
					ipcRenderer.send("config:save", config);
				}
			}}
			validate={({ host }) => {
				return {
					host: !host ? 'A host is required' : undefined
				}
			}} >
			{({submitForm}) => {
				return (
				<form className="settings-form style-1" onSubmit={submitForm}>
			  		<Text key='host' field='host' type='text' placeholder='Host' />
			  		<Text key='username' field='username' type='text' placeholder='Username' />
			  		<Text key='password' field='password' type='password' placeholder='Password' />
			  		<Text key='dbname' field='dbname' type='text' placeholder='DBNAME' />
				    <Select key='dialect'
				      field='dialect'
				      options={[{
				      	id: 1,
				        label: 'Microsoft SQL Server',
				        value: 'mssql'
				      }]}
				    />

			  		<button type="submit" className="btn">保存</button>
				</form>)
			}}

		</Form>
		</div>);
	}
}

