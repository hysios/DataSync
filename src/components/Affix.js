import React from 'react';
import ReactDOM from 'react-dom';

function cumulativeOffset (element) {
    var top = 0, left = 0;
    do {
        top += element.offsetTop  || 0;
        left += element.offsetLeft || 0;
        element = element.offsetParent;
    } while(element);

    return {
        top: top,
        left: left
    };
};

export default class Affix extends React.Component {
	constructor(props) {
		super(props)
		this.handleScrolled = this.handleScrolled.bind(this)
	}

	handleScrolled (event) {
		let dom = ReactDOM.findDOMNode(this)
		let scrollTop = document.body.scrollTop;

		if (scrollTop >= this.affixTop) {
			if (!this.affix) {
				this.affix = true
				this.oldPosition = dom.style.position
				this.oldTop =  cumulativeOffset(dom).top
				dom.style.position = 'fixed';
				dom.style.top = this.props.top + 'px'				
			}
		} else {
			if (this.affix) {
				this.affix = false
				dom.style.position = this.oldPosition 
				dom.style.top = this.oldTop
			}
		}
	}

	componentDidMount() {
		let dom = ReactDOM.findDOMNode(this)
		this.affixTop = cumulativeOffset(dom).top;
		window.addEventListener('scroll', this.handleScrolled)
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.handleScrolled)

	}

	render() {
		return <div>
			{this.props.children}
		</div>
	}
}

Affix.propTypes = {
	top: React.PropTypes.number
}