import React from 'react';
// eslint-disable-next-line
const remote = eval("require('electron').remote");
const {autoUpdater} = remote.require("electron-updater");
const {dialog} = remote;


export default class Upgrade extends React.Component {
	state = {}

	onUpdate(e) {
		e.preventDefault();
        autoUpdater.checkForUpdates();
	}

	componentDidMount() {
		this.setState({
			begin: false
		});

		autoUpdater.on('checking-for-update', () => {
			this.setState({
				message: '正在进行更新检查...'
			});
		});

		autoUpdater.on('update-available', (info) => {
			this.setState({
				message: '有效的更新.'
			});
		});

		autoUpdater.on('update-not-available', (info) => {
			this.setState({
				message: '没有有效的更新.'
			});
			dialog.showMessageBox({type: 'info', title: '更新', message: '没有新的可用更新。'})
		});

		autoUpdater.on('error', (err) => {
			this.setState({
				errorMessage: '更新发生错误.'
			});
		})

		autoUpdater.on('download-progress', (progressObj) => {
		  // let log_message = "Download speed: " + progressObj.bytesPerSecond;
		  // log_message = log_message + ' - Downloaded ' + progressObj.percent + '%';
		  // log_message = log_message + ' (' + progressObj.transferred + "/" + progressObj.total + ')';
		  this.setState({
		  	begin: true,
		  	precent: progressObj.precent
		  });
		})

		// autoUpdater.on('update-downloaded', (info) => {
		//   sendStatusToWindow('Update downloaded; will install in 5 seconds');
		// });

		autoUpdater.on('update-downloaded', (info) => {
		  // Wait 5 seconds, then quit and install
		  // In your application, you don't need to wait 5 seconds.
		  // You could call autoUpdater.quitAndInstall(); immediately
	   		autoUpdater.quitAndInstall();  
		})
		
	}

	componentWillUnmount() {
		autoUpdater.removeAllListeners('checking-for-update')
		autoUpdater.removeAllListeners('update-available')
		autoUpdater.removeAllListeners('update-not-available')
		autoUpdater.removeAllListeners('error')
		autoUpdater.removeAllListeners('download-progress')
		autoUpdater.removeAllListeners('update-downloaded')
	}

	render () {
		let {message, begin} = this.state;
		let msg = <p>{message}</p>;
		if (this.state.errorMessage) {
			msg = <p className="error">{this.state.errorMessage}</p>;
		}

		let progStyle = {
			display: begin ? "inline" : "none"
		}

		return (<div>
			<br/>
			<h3>当前版本：{remote.app.getVersion()}</h3>
			<button className="btn" onClick={this.onUpdate} >更新</button>
			<br/>
			{msg}
			<progress style={progStyle} value={this.precent} max={100}></progress>
		</div>);
	}
}


