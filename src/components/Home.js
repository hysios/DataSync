import React from 'react';
import Engine from '../engine';
import GreeterOrder from './GreeterOrder';
import {TransitionGroup} from 'react-transition-group' // ES6
import {API_SERVER} from '../constants';
import {Slide} from '../animations';
import NotificationSystem from 'react-notification-system';
import Affix from './Affix';
import FaPrint from 'react-icons/lib/fa/print';
import moment from 'moment';
import _ from 'lodash';

import Dropdown, {
    DropdownToggle,
    DropdownMenu,
    DropdownMenuWrapper,
    DropdownSubMenu,
    MenuItem,
    DropdownButton
} from '@trendmicro/react-dropdown';

import '@trendmicro/react-buttons/dist/react-buttons.css';
import '@trendmicro/react-dropdown/dist/react-dropdown.css';
moment.locale('zh-cn');

// class JobEvent extends EventEmitter {
// 	constructor(job) {
// 		super()
// 		this.job = job;
// 	}
// }

export default class Home extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			messages: [],
			groupMessages: {},
			sinceId: 0,
			limit: 10
		};
		this.createOrder = this.createOrder.bind(this);
		this.fetchData = this.fetchData.bind(this);
		this.handleWheel = this.handleWheel.bind(this);
		this.handlePrinter = this.handlePrinter.bind(this);
		this.handlePrintReturn = this.handlePrintReturn.bind(this);
		this.handlePrintStatus = this.handlePrintStatus.bind(this);
		this._defaultPrinter = null
		this.events = {};
	}

	fetchSource(options) {
		let headers = {
			'Accept': 'application/json, text/plain, */*',
		    'Content-Type': 'application/json'
		};

		return fetch(`${API_SERVER}/messages`,{
		    headers,
			method: 'POST', 
			body: JSON.stringify(options)
		}).then(response => {
			return response.json();
		})		
	}

	fetchData(options = {}) {
		let {sinceId, limit} = this.state;
		if (this.loading) {
			return;
		}

		this.loading = true;
		function buildMsg(row, options) {
			return {
				id: row.id,
				order: JSON.parse(row.body),
				init: options.init ? true: false
			};
		}

		this.fetchSource({sinceId, limit}).then(({nextCursor, objects}) => {
			let newMessages = objects.map(buildMsg);
			let { messages } = this.state;
			messages = messages.concat(newMessages).sort((a,b ) => b.id - a.id)
			// console.log(moment(messages[0].order.createdAt).format("YYYY MMM Do"))
			
			let groupMessages = this.groupMessages(messages)
			this.setState({
				messages,
				groupMessages,
				sinceId: nextCursor
			});
			this.loading = false;
		})		
	}

	groupMessages(messages) {
		return _.groupBy(messages, ({order}) => moment(order.createdAt).format("YYYY-MM-DD"))	
	}

	componentWillMount() {
		Engine.defaultPrinter()
        this.fetchData({init: true});		
		Engine.on('order:create', this.createOrder)
		Engine.on('printer:default', this.handlePrinter)
		Engine.on('order:print:reply', this.handlePrintReturn)
		Engine.on('order:print:status', this.handlePrintStatus)

	}

	componentWillUnmount() {
		Engine.removeListener('printer:default', this.handlePrinter)
		Engine.removeListener('order:create', this.createOrder)
		Engine.removeListener('order:print:reply', this.handlePrintReturn)
		Engine.removeListener('order:print:status', this.handlePrintStatus)
	}

	createOrder(message) {
		let {messages} = this.state;
		let newMessages = [message,...messages]

		this.setState({
			messages: newMessages,
			groupMessages: this.groupMessages(newMessages)
		})
	}

	handlePrinter(printer) {
		this._defaultPrinter = printer;
		this.setState({
			defaultPrinter: printer,
		})
	}

	handlePrintReturn(reply) {
		if (reply.error) {
			let {error} = reply;
			this.refs.notificationSystem.addNotification({
				message: error.message,
				level: 'error',
				dismissible: false
			});
		}


		console.log(reply)
	}

	handlePrintStatus(job) {
		let jobHasStatus = (job, status) => {
			return !!~job.job.status.indexOf(status)
		}

		if (jobHasStatus(job, 'CANCELLED')) {
			this.refs.notificationSystem.addNotification({
				message: `${job.jobId} 任务被取消`,
				level: 'warning',
				dismissible: false
			});
		} else if (jobHasStatus(job, 'PRINTING')) {
			// this.emit('PRINTING', job);
		} else if (jobHasStatus(job, 'PAUSED')) {
			// this.emit('PAUSED', job);
		}

		let messages = this.state.messages;
		let found = messages.findIndex((message) => message.id === job.order.id);
		if (found > -1) {
			let msg = messages[found]
			msg.status = job.job.status[0];
			this.state({
				messages: messages
			});
		}
	}

	// emit(obj, name) {
	// 	let job = this.events[obj.jobId]
	// 	if (!job) {
	// 		this.events[obj.jobId] = new JobEvent(obj)
	// 		job = this.events[obj.jobId]
	// 		job.once(name, this.handleStatus)
	// 	}

	// 	this.job.emit(name);
	// }

	handleWheel(event) {
		let {scrollTop, scrollHeight} = document.body;
		const prepareHeight = 150;
		if (scrollTop + window.innerHeight + prepareHeight >= scrollHeight) {
			this.fetchData();
		}
	}

	renderBatchPrint() {
		return <Dropdown
		    onSelect={(eventKey) => {
		    }}
			>
		    <Dropdown.Toggle
		        btnStyle="flat"
		    >
		        批量打印
		    </Dropdown.Toggle>
		    <Dropdown.Menu>
		        <MenuItem header>未打印</MenuItem>
		        <MenuItem eventKey={1}>今天</MenuItem>
		        <MenuItem eventKey={2}>三天内</MenuItem>
		        <MenuItem divider />
		        <MenuItem header>重新打印</MenuItem>
		        <MenuItem eventKey={3}>今天</MenuItem>
		        <MenuItem eventKey={4} disabled>disabled</MenuItem>
		        <MenuItem
		            eventKey={4}
		            title="选择时段打印"
		        >
		            选择时段打印
		        </MenuItem>
		        <MenuItem
		            eventKey={5}
		            href="#someHref"
		            active
		            onSelect={(eventKey) => {
		                alert(`Alert from menu item.\neventKey: ${eventKey}`);
		            }}
		        >
		            选择订单号打印
		        </MenuItem>
		    </Dropdown.Menu>
		</Dropdown>
	}

	render() {
		let {groupMessages, messages, defaultPrinter} = this.state;
		console.log(111+groupMessages, messages);

		return (<div className="container-flex" onWheel={this.handleWheel}>
			<div className="printList">

			{Object.keys(groupMessages).map((key) => {
				return <div className="messageGroup" key={key} >
					<div className="relative">
						<span className="colour-demo">
							<b className="new"><span className="line"></span>新单</b>
							<b className="saved"><span className="line"></span>保存</b>
							<b className="printed"><span className="line"></span>打印</b>
						</span>
					</div>
					{console.log(moment(key))}		
					<h1 className="order-title">{moment(key).isSame(new Date(), "day") ? "今日" : moment(key).format("YYYY MMM Do")}</h1>
		        	<TransitionGroup className='order-list'>
					{groupMessages[key].map((message) => {
						return (
							<Slide key={message.id} in={false} enter={message.init} >
								<GreeterOrder order={message.order} printer={defaultPrinter} status={message.status} />
							</Slide>
						);
					})}
			        </TransitionGroup>
				</div>
			})}
	        </div>
	        <div className="rightSidebar">
		         <Affix top={0}>
		         	<h3>打印管理</h3>
					<label className="switch">
					  <input type="checkbox" />
					  <span className="slider round"></span>
					</label>
					<span>自动打印</span>
					<br />
					<hr />
					{this.renderBatchPrint()}		
	            </Affix>
	        </div>
          <NotificationSystem ref="notificationSystem" position="tc" />
		</div>)
	}
}
