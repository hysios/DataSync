import React from 'react';
// import {
//   Route
// } from 'react-router-dom';

function WithRouter(ComponentClass) {
  return React.createClass({
    displayName: 'WithRouter',

    contextTypes: {
      router: React.PropTypes.func.isRequired,
    },

    render() {
      console.log(this.context)
      return <ComponentClass {...this.props} router={this.context.router}/>;
    }
  });
}
export default WithRouter;