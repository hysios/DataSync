import React from 'react';
import formatCurrency from 'format-currency';

export default class Currency extends React.Component {
	render() {
		let opts = { format: '%s%v', symbol: '$' };
		return <span className="currency">{formatCurrency(this.props.value, opts)}</span>
	}
}

Currency.propTypes = {
	value: React.PropTypes.any
}