import React from 'react';
import Table from './Table';
import requestData from '../requestData';
import NotificationSystem from 'react-notification-system';

export default class DataTabler extends React.Component {
    constructor(props) {
		super(props);
		this.state = Object.assign(this.state || {}, {
			data: [],
			pages: null,
      		loading: true,
      		height: window.innerHeight - 180
		})
	    this.fetchData = this.fetchData.bind(this);
	    this.fetchCatch = this.fetchCatch.bind(this);
	    this.fetchError = this.fetchError.bind(this);
	    // this.onResize = this.onResize.bind(this);
	}

	checkStatus(response) {
		if (response.status >= 200 && response.status < 500) {
			return response
		} else {
			var error = new Error(response.statusText)
			error.response = response
			throw error
		}
	}

	fetchSource() {
		return Promise.resolve({rows: [], count: 0});
	}

	fetchError(result) {
		if (result && result.error) {
			console.log(result.error)
			throw result.error;
		} else {
			return result;
		}
	}

	fetchCatch(err) {
		this.refs.notificationSystem.addNotification({
			message: err.message,
			level: 'error',
			dismissible: false
		});
	}

	fetchData(state, instance) {
	    // Whenever the table model changes, or the user sorts or changes pages, this method gets called and passed the current table model.
	    // You can set the `loading` prop of the table to true to use the built-in one or show you're own loading bar if you want.
	    this.setState({ loading: true });
	    this.fetchSource({
	    	offset: state.page*state.pageSize, 
	    	limit: state.pageSize
	    })
    	.then(requestData(state.pageSize,
			state.page,
			state.sorted,
			state.filtered))
    	.then(res => {
			// Now just get the rows of data to your React Table (and update anything else like total pages or loading)
			setImmediate(() => {
				this.setState({
					data: res.rows,
					pages: res.pages,
					loading: false
				}); 
			})   			
       	})
    	.catch(this.fetchCatch)
	}

	// onResize() {
	// 	this.setState({
	// 		height: window.innerHeight - 180
	// 	});
	// }

	render() {
		// let {data} = this.state;    
		const { data, pages, loading } = this.state,
			{ columns } = this.props;

		return (<div className="container">
			<Table
	          	data={data}
	          	columns={columns}
	          	manual
  	          	pages={pages} // Display the total number of pages
          		loading={loading} // Display the loading overlay when we need it
	          	defaultPageSize={10}
				onFetchData={this.fetchData} // Request new data when things change
	          	className="-striped -highlight"
	        />
            <NotificationSystem ref="notificationSystem" position="tc" />
		</div>)
	}
}

DataTabler.defaultProps = {
  	columns: []
};