import React from 'react';
import DataTabler from './DataTabler';
import Currency from './Currency';
import Number from './Number';
import {API_SERVER} from '../constants';

export default class Inventory extends DataTabler {
	fetchSource(options = {}) {
		let headers = {
			'Accept': 'application/json, text/plain, */*',
		    'Content-Type': 'application/json'
		};
		return fetch(`${API_SERVER}/inventory`,{
		    headers,
			method: 'POST', 
			body: JSON.stringify(options)
		}).then(response => {
			return response.json();
		})
	}
}

Inventory.defaultProps = {
	columns:  [
		{
			Header: "货品编码",
			accessor: "goods_code",
		},
		{
			Header: "货品名称",
			accessor: "goods_name",
		},
		{
			Header: "规格",
			accessor: "specs",
			style: {textAlign: 'center'},
		},		            
		{
			Header: "单位",
			accessor: "base_unitname", 
		},
		{
			Header: "期初结存数量",
			accessor: "init_qty",
			style: {textAlign: 'right'},
			Cell: row => (
				<Number value={row.value} />
			)					
		},		
		{
			Header: "本期收入数量",
			accessor: "base_inqty",
			style: {textAlign: 'right'},
			Cell: row => (
				<Number value={row.value} />
			)					
		},
		{
			Header: "本期发出数量",
			accessor: "base_outqty",
			style: {textAlign: 'right'},
			Cell: row => (
				<Number value={row.value} />
			)					
		},
				
		{
			Header: "本期结存数量",
			accessor: "qty",
			style: {textAlign: 'right'},
			Cell: row => (
				<Number value={row.value} />
			)
		},
	],
	columnsDetail: [
		{
			Header: "货品编码",
			accessor: "goods_code",
		},
		{
			Header: "货品名称",
			accessor: "goods_name",
		},
		{
			Header: "规格",
			accessor: "specs",
		},		            
		{
			Header: "单位",
			accessor: "base_unitname", 
		},
		{
			Header: "本期收入",
			columns: [
				{
					Header: "数量",
					accessor: "base_inqty",
					style: {textAlign: 'right'},
					Cell: row => (
						<Number value={row.value} />
					)					
				},
				{
					Header: "金额",
					accessor: "inamt",
	            	style: {textAlign: 'right'},
					Cell: row => (
						<Currency value={row.value} />
					)

				}
			]
		},
		{
			Header: "本期发出",
			columns: [
				{
					Header: "数量",
					accessor: "bill_outqty",
					style: {textAlign: 'right'},
					Cell: row => (
						<Number value={row.value} />
					)					
				},
				{
					Header: "金额",
					accessor: "outamt",
	            	style: {textAlign: 'right'},
					Cell: row => (
						<Currency value={row.value} />
					)

				}
			]
		},
		{
			Header: "本期结存",
			columns: [
				{
					Header: "数量",
					accessor: "qty",
					style: {textAlign: 'right'},
					Cell: row => (
						<Number value={row.value} />
					)
				},
				{
					Header: "金额",
					accessor: "amount",
	            	style: {textAlign: 'right'},
					Cell: row => (
						<Currency value={row.value} />
					)
				}
			]
		}		
	]
}