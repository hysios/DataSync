import React from 'react';
import Engine from '../engine';
import FaPrint from 'react-icons/lib/fa/print';
import FaCheck from 'react-icons/lib/fa/check';

export default class Printer extends React.Component {
	constructor(props) {
		super(props)
		this.getPrinters = this.getPrinters.bind(this);
		this.state = {
			printers: []
		}
	}

	componentWillMount() {
		Engine.getPrinters()
		Engine.on('printers:all', this.getPrinters)
	}

	componentWillUnmount() {
		Engine.removeListener('printers:all', this.getPrinters)
	}

	getPrinters(printers) {
		this.setState({
			printers: printers
		})
	}

	handleClickPrinter(printer) {
		let dlgOpt = {
			type: 'info', 
			buttons: ['确定', '取消'], 
			title: '选择打印机',
			defaultId: 0, 
			message: '选择当前 ' + printer.name + ' 打印机吗？'
		};

		Engine.showMessage(dlgOpt, (btn) => {
			if (btn === 0) {
				Engine.send('printer:selected', printer);
			}
		})
	}

	render() {
		let {printers} = this.state;
		let styles = {
			NORMAL: {},
			STOPPED: {
				color: 'red'
			}
		}

		let hasJobs = (printer) => {
			if (printer.jobs && printer.jobs.length > 0) {
				return <span> 共有 {printer.jobs.length} 作业</span>
			} else {
				return <span />
			}
		}

		let printerStatus = (printer) => {
			console.log(printer.status);
			switch(printer.status) {
			case 'IDLE':
				return '空闲中'
			case 'STOPPED':
				return '停止'
			default:
				return ''
			}
		}

		let printerStyle = (printer) => {
			switch(printer.status) {
			case 'IDLE':
				return styles.NORMAL
			case 'STOPPED':
				return styles.STOPPED
			default:
				return styles.NORMAL
			}			
		}

		let isChoosePrinter = (printer) => {
			let {defaultPrinter} = Engine.getConfig();
			if (defaultPrinter) {
				return defaultPrinter.name === printer.name;
			} else {
				return printer.isDefault
			}
		}

		return <div className="container">
			<div className="row">
				<div className="col-6">
					<div className="printer">
						<div className="printer-icon">
							<FaPrint />
						</div>
						<div className="printer-content">
							<h3>TEST</h3>
							<p></p>
						</div>
					</div>
				</div>
			{printers.map(printer => {
				return <div key={printer.name} className="col-6" onClick={this.handleClickPrinter.bind(this, printer)} > 
					<div className="printer">
						<div className="printer-icon">
							<FaPrint />
						</div>
						<div className="printer-content">
							<h3>{printer.name}</h3>
							<p style={printerStyle(printer)} >{printerStatus(printer)}</p>
							{ hasJobs(printer) }
						</div>
						<div className="printer-status">
							{ isChoosePrinter(printer) ? <FaCheck />: null}
						</div>
					</div>				
				 </div>
			})}
			</div>
		</div>
	}
}