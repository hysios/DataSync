import React from 'react';
import moment from 'moment';
import ReactDOM from 'react-dom';
moment.locale('zh-cn');

export default class Currency extends React.Component {

	constructor(props) {
		super(props)
		this.tick = this.tick.bind(this);
	}

	componentWillMount() {
		this.clockId = setInterval(this.tick, 1000);
	}

	componentWillUnmount() {
		clearInterval(this.clockId);
	}

	tick() {
		let dom = ReactDOM.findDOMNode(this);
		dom.childNodes[0].nodeValue = moment(this.props.value).fromNow();
	}

	render() {
		let time = moment(this.props.value);
		return <span title={time.format("YYYY MMM Do HH:mm:ss")}>{time.fromNow()}</span>;
	}
}

Currency.propTypes = {
	value: React.PropTypes.any
}