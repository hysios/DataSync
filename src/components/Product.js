import React from 'react';
import Currency from './Currency';
import DataTabler from './DataTabler';
import {API_SERVER} from '../constants';

export default class Product extends DataTabler {

	fetchSource(options = {}) {
		return fetch(`${API_SERVER}/products`,{
		    headers: {
			   	'Accept': 'application/json',
			    'Content-Type': 'application/json'
			},
			method: 'POST', 
			body: JSON.stringify(options)
		})
		.then(this.checkStatus)
		.then(response => {

			return response.json()
		})
	}
}

Product.defaultProps = {
	columns: [
		{
			Header: "商品编码",
			accessor: "code",
		},
		{
			Header: "货品名称",
			accessor: "name",
		},
		{
			Header: "规格",
			accessor: "specs",
			style: {textAlign: 'center'},
		},		            
		{
			Header: "单位",
			accessor: "unit", 
			style: {textAlign: 'center'},
		},
		{
			Header: "进货价",
			accessor: "aprice",
			style: {textAlign: 'center'},
			Cell: row => (
				<Currency value={+row.value} />
			)
		}
	]
}
