import React from 'react';
import {
  Link
} from 'react-router-dom'
import DataTabler from './DataTabler';
import Date from './Date';
import Currency from './Currency';
import {API_SERVER} from '../constants';

export default class Purchase extends DataTabler {

	fetchSource(options = {}) {
		let headers = {
			'Accept': 'application/json, text/plain, */*',
		    'Content-Type': 'application/json'
		};
		return fetch(`${API_SERVER}/purchase`,{
		    headers,
			method: 'POST', 
			body: JSON.stringify(options)
		}).then(response => {
			return response.json()
		})
	}
}

Purchase.defaultProps = {
	columns: [
        {
        	Header: "单号",
        	accessor: "code",
            style: {textAlign: 'left'},
        },
        {
        	Header: "下单时间",
        	accessor: "billdate",
            style: {textAlign: 'center'},
        	Cell: row => (
        		<Date value={row.value} />
        	)
        },
            {
        	Header: "供应商",
        	accessor: "client.shortname",
            style: {textAlign: 'center'},
        },
        {
        	Header: "总金额",
        	accessor: "totalamt",
            style: {textAlign: 'right'},
            Cell: row => (
                <Currency value={row.value} />
            )
        },
        {
        	Header: "工具",
        	accessor: "code",
            style: {textAlign: 'center'},
        	Cell: row => (
	          <Link className="btn-grad" to={'/purchase/' + row.value} >打开</Link>
         	)   
        }
  	]
};
