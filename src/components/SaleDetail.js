import React from 'react';
import Currency from './Currency';
import moment from 'moment';
import numeral from 'numeral';
import formatCurrency from 'format-currency';
import DataTabler from './DataTabler';
import Table from './Table';
import NotificationSystem from 'react-notification-system';
import {API_SERVER} from '../constants';


export default class SaleDetail extends DataTabler {
	state = {
		sale: {client: {}, store: {}, employ: {}},
	}

	fetchSource(options = {}) {
		let {params} = this.props.match,
			headers = {
			   	'Accept': 'application/json, text/plain, */*',
			    'Content-Type': 'application/json'
			};

		return fetch(`${API_SERVER}/sales/${params.code}`,{
		    headers,
			method: 'POST', 
			body: JSON.stringify(options)
		}).then(response => {
			return response.json()
		}).then(this.fetchError)
		.then((result) => {
    		this.setState({sale: result});
    		return result.saleDetails;
    	})
	}

	quantityTotal() {
		let {sale} = this.state, 
			total = sale.saleDetails ? sale.saleDetails.quantityTotal : 0;
		return numeral(total).format('0,0')
	}

	amountTotal() {
		let {sale} = this.state, 
			opts = { format: '%s%v', symbol: '$' },
			total = sale.saleDetails ? sale.saleDetails.amountTotal : 0;
		return formatCurrency(total, opts);
	}

	render() {
		const { sale, data, pages, loading} = this.state,
		columns = [
            {
            	Header: "商品编码",
            	accessor: "good.code",
            },	          	
            {
            	Header: "商品名称",
            	accessor: "good.name",
            	Footer: "合计",
            	minWidth: 200
            },
            {
            	Header: "条型码",
            	accessor: "goodunit.barcode",
            	width: 120,
                style: {textAlign: 'center'},
            },	            
            {
            	Header: "规格",
            	accessor: "good.specs",
                style: {textAlign: 'center'},
            },
            {
            	Header: "单位",
            	accessor: "goodunit.unitname",
            	width: 50,
                style: {textAlign: 'center'},
            },	    	           	            
            {
            	Header: "数量",
    	        id: 'quantity',
            	accessor: d => d.quantity / d.goodunit.rate,		            	
            	style: {textAlign: 'center'},
            	Footer: (
            		<span>{this.quantityTotal()}</span>
            		)
            },
            {
            	Header: "单价",
            	accessor: "unitprice",
            	style: {textAlign: 'right'},
            	Cell: row => (
					<Currency value={+row.value} />
            	)
            },
            {
            	Header: "总计",
            	accessor: "amount",
            	style: {textAlign: 'right'},
            	Cell: row => (
					<Currency value={+row.value} />
            	),
            	Footer: (
            		<span>{this.amountTotal()}</span>
            	)
            },
            {
            	Header: "备注",
            	accessor: "memo"
            }
        ];

        let employ = (sale || {}).employ || {};
	
		return (<div className="container">
			<ul className="saleHeader">
				<li>客户名称：{sale.client.shortname}</li>
				<li>客户电话：{sale.phone}</li>
				<li>单号: {sale.code}</li>
			</ul>
			<ul className="saleHeader">
				<li>送货地址：{sale.shipto}</li>
				<li>出货仓: {sale.store.name}</li>
				<li>开单日期: {moment(sale.billdate).format("YYYY MMM Do")}</li>
			</ul>
            <ul className="saleHeader">
                <li>业务员: {employ.name}</li>
                <li>联系电话: {employ.mobilephone || employ.telephone}</li>
                <li></li>
            </ul>
			<Table
	          	data={data}
	          	columns={columns}
	          	manual
  	          	pages={pages} // Display the total number of pages
          		loading={loading} // Display the loading overlay when we need it
	          	defaultPageSize={10}
				onFetchData={this.fetchData} // Request new data when things change
	          	className="-striped -highlight"
	        />
            <NotificationSystem ref="notificationSystem" position="tc" />
		</div>)
	}
}
