import React from 'react';
import numeral from 'numeral';

export default class Number extends React.Component {
	render() {
		return <span className="number">{numeral(this.props.value).format('0,0')}</span>;
	}
}

Number.propTypes = {
	value: React.PropTypes.any
};