import React from 'react'
import { CSSTransition } from 'react-transition-group' // ES6

const Fade = ({ children, ...props }) => (
 <CSSTransition
    {...props}
    timeout={1000}
    classNames="order"
  >
    {children}
  </CSSTransition>
);

const Slide = ({ children, ...props }) => (
 <CSSTransition
    {...props}
    timeout={1000}
    classNames="slidedown"
  >
    {children}
  </CSSTransition>
);

export {
  Fade,
  Slide
}