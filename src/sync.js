const {
	createQueue,
	sendMessage,
	receiveMessage,
	deleteMessage,
	messageLoop
} = require('./sqs');
const log = require('./log');
const http = require('http');
const getManager = require('./queue/commandManager');
const request = require('./request');
const {
	COMMAND_QUEUE, 
	ITEM_MAX_DISPLAY, 
	GIFT_MAX_DISPLAY,
	EMPTY
} = require('./constants');
const {Notification} = require('electron');
const {
	Invoice,
	Message, 
	Client, 
	Operator, 
	Employ,
	Good,
	GoodUnit,
	SaleDetail,
	sequelize,
	Sequelize
} = require('./models');
const Op = Sequelize.Op;
const {getAttr} = require('./queue/utils');
const {isEmpty} = require('./utils');
const errors = require('./errors');
const util = require('util');
const getOrderManager = require('./order').getManager;

let receUrl, sendUrl;


module.exports = function syncMain(app) {
	let orderManager = getOrderManager({app: app});
	// orderManager.run();

	return function() {
		const manager = getManager(COMMAND_QUEUE);

		manager.on('command:products', (msg) => {
			let opt = Object.assign({
				method: 'POST', 
				path: '/products'
			}, JSON.parse(msg.getData()));
			log.info(msg.getData())
			log.info('receive command products', opt);
			request(opt).then((products) => msg.reply(products));
		});

		manager.on('command:customers', (msg) => {
			let opt = Object.assign({
				method: 'POST', 
				path: '/customers'
			}, JSON.parse(msg.getData()));
			log.info(msg.getData())
			log.info('receive command customers', opt);
			request(opt).then((customers) => msg.reply(customers));
		});

		manager.on('command:units', (msg) => {
			let opt = Object.assign({
				method: 'POST', 
				path: '/goodsunits'
			}, JSON.parse(msg.getData()));
			log.info(msg.getData())
			log.info('receive command units', opt);
			request(opt).then((units) => msg.reply(units));
		});

		manager.on('command:create_client', (msg, meta) => {
			let opt = Object.assign({
				method: 'POST', 
				path: '/customers/create'
			}, JSON.parse(msg.getData()));
			log.info('receive command create client', opt);
			delete opt['isdouble'];
			request(opt).then((customer) => msg.reply(customer)).catch(err => reply({error: err}))
		})

		manager.on('command:employees', (msg) => {
			let opt = Object.assign({
				method: 'POST', 
				path: '/employee'
			}, JSON.parse(msg.getData()));
			log.info(msg.getData())
			log.info('receive command employees', opt);
			request(opt).then((employees) => msg.reply(employees));
		});

		manager.on('command:create_employee', (msg) => {
			let opt = Object.assign({
				method: 'POST', 
				path: '/employee/create'
			}, JSON.parse(msg.getData()));
			log.info('receive command create employee', opt);
			request(opt).then((employ) => msg.reply(employ)).catch(err => reply({error: err.message}))
		});
		
		manager.on('command:create_order', (msg, meta) => {
			log.info('receive create order', meta);
			orderManager.push(msg, meta)
		})

		manager.listen();
	}
}

