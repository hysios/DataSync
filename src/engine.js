const EventEmitter = require('events');
// eslint-disable-next-line
const {ipcRenderer, remote} = eval("require('electron')");

class Engine extends EventEmitter {
	constructor() {
		super()

		this.createdOrder = this.createdOrder.bind(this);
		ipcRenderer.on('order:create', this.createdOrder);
		ipcRenderer.on('printers:all', (event, printers) => this.emit('printers:all', printers))
		ipcRenderer.on('printer:default', (event, printer) => this.emit('printer:default', printer))
		ipcRenderer.on('order:print:reply', (event, reply) => this.emit('order:print:reply', reply))
		ipcRenderer.on('order:print:status', (event, job) => this.emit('order:print:status', job))
	}

	showMessage(options, callback) {

		remote.dialog.showMessageBox(remote.getCurrentWindow(), options, callback)
	}

	createdOrder(event, order) {
		this.emit('order:create', order);
	}

	getPrinters() {
		this.send('printers:get')
	}

	defaultPrinter() {
		this.send('printer:get_default')	
	}

	getConfig() {
		return remote.getGlobal('config') || {};
	}

	send(message, ...args) {
		ipcRenderer.send(message, ...args)
	}
}

const _engine = new Engine();

module.exports = _engine;