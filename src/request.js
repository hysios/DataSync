const http = require('http');
const log = require('./log');
const {API_SERVER} = require('./constants');
const url = require('url');

module.exports = function request(options, callback) {
	return new Promise((resolve, reject) => {
		log.info('request', options)

		let {path, method, offset, limit, body} = options;
		let postData = JSON.stringify({
		  offset, limit, body
		});
		log.info('postData', postData)
		let u = url.parse(API_SERVER);
		log.info('api server', u)

		let reqOpt = {
			port: 3001,
			hostname: '127.0.0.1',
			path,
			method,
			headers: {
				'Accept': 'application/json',
				'Cache-Control': 'no-cache',
				'Content-Type': 'application/json; charset=utf-8',
				'Content-Length': Buffer.byteLength(postData)
			}
		};
		log.info('request options', reqOpt)

		const req = http.request(reqOpt, (res) => {
			let buf = ''
			res.setEncoding('utf8');
			res.on('data', (chunk) => {
				buf += chunk
			});
			res.on('end', () => {
				log.info('command reply', buf.length);
				try {
					resolve(JSON.parse(buf))
				} catch (err) {
					reject(err)
				}
			});
		});

		req.on('error', (e) => {
		  log.error(`problem with request: ${e.message}`);
		  reject(e);
		});

		// write data to request body
		req.write(postData);
		req.end();
	});
}