const Command = require('../queue/command');
const util = require('util');
const {printTable} = require('../utils');
const {COMMAND_QUEUE} = require('../constants');

const createClient = new Command('create_client', {
	namespace: COMMAND_QUEUE
});

createClient.send({
		body: { 
			clientkind: 1,
	    	isdouble: false,
			mobilephone: '',
			name: '二中冰河店',
			shortname: '冰河'
		}
	}).then(result => {
		console.log('result', result)
	})