const Command = require('../queue/command');
const util = require('util');
const {printTable} = require('../utils');
const {COMMAND_QUEUE} = require('../constants');

const getProducts = new Command('products', {
	method: 'POST',
	path: '/products',
	namespace: COMMAND_QUEUE
});

const getCustomers = new Command('customers', {
	method: 'POST',
	path: '/customers',
	namespace: COMMAND_QUEUE
});

const getUnits = new Command('units', {
	method: 'POST',
	path: '/goodsunits',
	namespace: COMMAND_QUEUE
});

getProducts
	.send({offset: 0, limit: 10})
	.then((result) => {
		let {data} = result,
			res = JSON.parse(data);

		if (res.error) {
			console.log(res);
			throw res.error
		}
		console.log('产品')
		printTable(res.rows, [
			{ title: 'ID', column: 'goodsid'},
			{ title: '代码', column: 'code'},
			{ title: '名称', column: 'name'},
			{ title: '单位', column: 'unit'},
			{ title: '规格', column: 'specs'},
			{ title: '价格', column: 'aprice'}
		])		
		// console.log(util.inspect(res, {showHidden: true, depth: 2, showProxy: true}));
	})
	.catch((err) => {
		console.error(err, err.stack);
	})

getCustomers
	.send({offset: 0, limit: 10})
	.then((result) => {
		let {data} = result,
			res = JSON.parse(data);

		if (res.error) {
			throw Error(res.error)
		}
		console.log('客户')
		printTable(res.rows, [
			{ title: 'ID', column: 'clientid'},
			{ title: '代码', column: 'code'},
			{ title: '简称', column: 'shortname'},
			{ title: '费用', column: 'balance'},
			{ title: '联系人', column: 'contator'},
			{ title: '手机', column: 'mobilephone'},
			{ title: '地址', column: 'address'}
		])
		// console.log(util.inspect(res, {showHidden: true, depth: 2, showProxy: true}));
	})
	.catch((err) => {
		console.error(err, err.stack);
	})


getUnits
	.send({offset: 0, limit: 10})
	.then((result) => {
		let {data} = result,
			res = JSON.parse(data);

		if (res.error) {
			throw Error(res.error)
		}
		console.log('单位')
		printTable(res.rows, [
			{ title: 'ID', column: 'unitid'},
			{ title: '产品ID', column: 'goodsid'},
			{ title: '单位', column: 'unitname'},
			{ title: '条码', column: 'barcode'},
			{ title: '比率', column: 'rate'},
			{ title: '进价', column: 'pprice'},
			{ title: '销价', column: 'sprice'},

		])
		// console.log(util.inspect(res, {showHidden: true, depth: 2, showProxy: true}));
	})
	.catch((err) => {
		console.error(err, err.stack);
	})

