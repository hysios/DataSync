const Command = require('../queue/command');
const util = require('util');
const {printTable} = require('../utils');
const {COMMAND_QUEUE} = require('../constants');

const {generateOrder} = require('./order_test');

const createOrder = new Command('create_order', {
	namespace: COMMAND_QUEUE
});

createOrder.send(generateOrder({randomStatus: false})).then(result => {
	console.log('result', result)
})