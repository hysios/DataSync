const hamming = require('compute-hamming');
const simhash = require('simhash')();
const {Message} = require('../models');


Message.findAll().then(messages => {
	for (let i = 0;i < messages.length -1; i++) {
		for (let j = 1; j< messages.length; j++) {
			let aobj = JSON.parse(messages[i].body), bobj = JSON.parse(messages[j].body);
			let am = msg(aobj), bm= msg(bobj);
			// console.log(am.join(','), bm.join(','))
			let ah = simhash(am), bh = simhash(bm);
			let dist = hamming(ah, bh);
			if (dist < 2 && dist > 0) {
				console.log(am.toString())
				console.log( bm.toString(), dist)
				console.log()
			}

			// process.stdout.write(dist)
		}
		console.log('');
	}
})


function msg(obj) {
	return [
		obj.address || '',
		obj.customerName || '',
		obj.contact || '',
		obj.storehouseName || '',
		(obj.originalTotal || 0)  || '',
		...msgItems(obj)
	]
}

function msgItems(obj) {
	let items = obj.items.map(item => {
		return [
			item.barcode || '',
			item.spec || '',
			item.productName || '',
			item.quantity + '',
			item.unit || '',
			(item.price ||0) + ''
		]
	})
	let gifts = (obj.gifts || []).map(item => {
		return [
			item.barcode || '',
			item.spec || '',
			item.productName || '',
			item.quantity + '',
			item.unit || '',
			(item.price ||0) + ''
		]
	}) 

	items = items.concat(gifts);
	return items.reduce((s, m) => s.concat(m), [])
}