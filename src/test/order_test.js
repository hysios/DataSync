const prod = require('./products.json');
const products = prod.rows;
const cust = require('./customers.json');
const customers = cust.rows;

const units = [
	'提',
	'件',
	'箱'
];

const status = [
	'new',
	'saved',
	'printed'
];

function sampleUnit() {
	return units[Math.floor(Math.random() * units.length)]
}

function sampleProduct(options = {}) {
	let sample = Math.floor(Math.random() * products.length)
	let raw = products[sample];
	let product = {
		goodsId: raw.goodsid,
		productId: raw.goodsid,
		productName: raw.name,
		barcode: raw.barcode || '6907992513829',
		spec: raw.specs,
		quantity: 5 + Math.floor(Math.random() * 20),
		unitName: sampleUnit(),
		price: raw.aprice
	} 
	return product
}

function sampleProducts(count, options = {}) {
	let products = [];
	for (let i = 0; i < count; i++) {
		products.push(sampleProduct(options));
	}

	return products;
}

function sampleCustomer() {
	let sample = Math.floor(Math.random() * customers.length);
	return customers[sample];
}

function sampleStatus() {
	let sample = Math.floor(Math.random() * status.length);
	return status[sample];
}

let customer = sampleCustomer()

function generateOrder(options = {}) {
	let items = sampleProducts(Math.random() * 15, options);

	return {
		orderId: (new Date).getTime(),
		address: '马王堆路',
		customerId: customer.clientid,
		customerName: customer.shortname,
		// deliveryTime(string of datetime)
		// originalTotal(float64)
		total: items.reduce((s, i) => (s += i.quantity), 0),
		status: options.randomStatus ? sampleStatus(): 'new',
		unit: '提',
		createdAt: new Date(),
		// storehouseId(int)
		storehouseName: '总仓',
		// contact(string)
		// salerId(int)
		salerName: '张明',
		operatorName: '匡风娟',
		items: items,
		gifts: [
		]
	}
}
module.exports = {
	generateOrder
}
