const AWS = require('aws-sdk');
const path = require('path');
const util = require('util');
const merge = require('merge-deep');
const uuid = require('uuid/v4');
AWS.config.loadFromPath(path.join(__dirname, './aws.json'));
let sqs = new AWS.SQS({apiVersion: '2012-11-05'});

function createQueue(queueName) {
	let params = {
		QueueName: queueName,
		Attributes: {
			'ReceiveMessageWaitTimeSeconds': '20',
		}
	};

	if (path.extname(queueName) === '.fifo'){
		params['Attributes']['FifoQueue'] = 'true';
		params['Attributes']['ContentBasedDeduplication'] = 'true';
	}

	return new Promise((resolve, reject) => {
		sqs.createQueue(params, (err, data) => {
			if (err) {
				reject(err)
			} else {
				resolve(data.QueueUrl);
			}			
		})
	});
}

function sendMessage(url, msg) {
	let params = sendParams(url, msg);

	return new Promise((resolve, reject) => {
		sqs.sendMessage(params, function(err, data) {
			if (err) {
				reject(err);
			} else {
				resolve(data.MessageId);
			}
		});	
	})
}

function sendMessageBuf(url, msg) {
	let buf = JSON.stringify(msg.body)
	if (buf.length > 250000) {
		let count = buf.length / 250000,
			parts = [];
		for (let i = 0; i < count; i++) {
			let part = buf.slice(i*25000, (i+1) *25000);
			parts.push(sendMessagePart(url, msg, i, count, part));
		}
		return Promise.all(parts);
	} else {
		return sendMessage(url, msg)
	}
}

function sendParams(url, msg, merged = () => {}) {
	let params = {
		DelaySeconds: 0,
		MessageAttributes: {
			MsgID: {
				DataType: "String",
				StringValue: msg.id,
			},
			Command: {
				DataType: "String",
				StringValue: msg.command
			}
		},
		MessageBody: msg.body,
		QueueUrl: url
	};

	if (msg.type) {
		params["MessageAttributes"]["Type"] = {
			DataType: "String",
			StringValue: msg.type
		}		
	}

	if (msg.returnURL) {
		params["MessageAttributes"]["ReturnURL"] = {
			DataType: "String",
			StringValue: msg.returnURL
		}			
	}

	if (msg.bodyAllSum) {
		params["MessageAttributes"]["BodyALLSum"] = {
			DataType: "String",
			StringValue: msg.bodyAllSum
		}		
	}	

	return merge(params, merged(params));
}

function sendMessagePart(url, msg, index, count, msgPart) {
	let params = sendParams(url, msg, (params) => {
		return {
			MessageAttributes: {
				MsgPart: {
					DataType: "Number",
					StringValue: '' + index,
				},
				MsgPartCount: {
					DataType: "Number",
					StringValue: '' + Math.ceil(count),
				},			
			},
			MessageBody: msgPart
		}
	});

	return new Promise((resolve, reject) => {
		sqs.sendMessage(params, function(err, data) {
			if (err) {
				reject(err);
			} else {
				resolve(data.MessageId);
			}
		});	
	})	
}

function sendBatchParams(url, msg, parts, options= {eachMerged: () => {}, merged: () => {}}) {
	let {eachMerged, merged} = options;
	let entries = parts.map((part, i) => {
		let params = sendParams(url, msg, (_) => {
			let p = {
				Id: uuid(),
				MessageAttributes: {
					MsgPart: {
						DataType: "Number",
						StringValue: '' + i,
					},
					MsgPartCount: {
						DataType: "Number",
						StringValue: '' + parts.length,
					},			
				},
				MessageBody: part
			}

			if (msg.type) {
				p["MessageAttributes"]["Type"] = {
					DataType: "String",
					StringValue: msg.type
				}		
			}

			if (msg.returnURL) {
				p["MessageAttributes"]["ReturnURL"] = {
					DataType: "String",
					StringValue: msg.returnURL
				}			
			}

			if (msg.bodyAllSum) {
				p["MessageAttributes"]["BodyALLSum"] = {
					DataType: "String",
					StringValue: msg.bodyAllSum
				}		
			}

			return merge(p, eachMerged(p));
		})
		delete params['QueueUrl'];
		return params;
	});

	let params = {
		Entries: entries,
		QueueUrl: url
	};

	return merge(params, merged(params));	
}

function sendMessageBatch(url, msg, parts) {
	let params = sendBatchParams(url, msg, parts);

	return new Promise((resolve, reject) => {
		sqs.sendMessageBatch(params, (err, data) => {
			if (err) {
				reject(err);
			} else {
				resolve(data);
			}
		});
	})
}

function receiveMessage(url) {
	let params = {
		AttributeNames: [
			"SentTimestamp"
		],
		MaxNumberOfMessages: 10,
		MessageAttributeNames: [
			"All"
		],
		QueueUrl: url,
		WaitTimeSeconds: 20
	};

	return new Promise((resolve, reject) => {
		sqs.receiveMessage(params, (err, data) => {
	  		if (err) {
	    		reject(err);
	  		} else {
	    		resolve(data);
		  	}
		});	
	});
}

function receiveMessageBuf(url) {
	let params = {
		AttributeNames: [
			"SentTimestamp"
		],
		MaxNumberOfMessages: 10,
		MessageAttributeNames: [
			"All"
		],
		QueueUrl: url,
		WaitTimeSeconds: 20
	};

	return new Promise((resolve, reject) => {
		sqs.receiveMessage(params, (err, data) => {
	  		if (err) {
	    		reject(err);
	  		} else {
	  			if (data.Messages && data.Messages.length > 0) {
	  				for (let i = 0; i < data.Messages.length; i++) {
	  					let msg = data.Messages[i],
	  						attributes = msg.MessageAttributes;
	  					if (attributes.MsgPart && attributes.MsgPartCount) {

	  					}
	  				}
	  			}
	    		resolve(data);
		  	}
		});	
	});	
}

function deleteMessage(url, message) {
	var params = {
		QueueUrl: url,
		ReceiptHandle: message.ReceiptHandle,
	};

	return new Promise((resolve, reject) => {
		sqs.deleteMessage(params, function(err, data) {
	  		if (err) {
	  			reject(err);
	  		} else {
	  			resolve(data);
	  		}
		});	
	});
}

function deleteMessageBatch(url, messages) {
	var params = {
	  Entries: messages,
	  QueueUrl: url /* required */
	};

	return new Promise((resolve, reject) => {
		sqs.deleteMessageBatch(params, function(err, data) {
	  		if (err) {
	  			reject(err);
	  		} else {
	  			resolve(data);
	  		}
		});	
	});
}

function deleteQueue(url) {
	var params = {
		QueueUrl: url
	};

	return new Promise((resolve, reject) => {
		sqs.deleteQueue(params, function(err, data) {
			if (err) {
				reject(err);
			} else {
				resolve(data);
			}
		});
	});
}


async function messageLoop(url, handle) {
	while(1) {
		let data = await receiveMessage(url)
		if (typeof handle === 'function' ) {
			if (handle(data)) {
				// for (let i in data.Messages) {
				// 	let message = data.Messages[i];
				// 	await deleteMessage(url, message);
				// }
			}
		}
	}
}

module.exports = {
	createQueue,
	sendMessage,
	sendMessageBuf,
	sendMessagePart,
	sendMessageBatch,
	receiveMessage,
	deleteMessage,
	deleteMessageBatch,
	deleteQueue,
	messageLoop
}