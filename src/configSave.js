
const path = require('path');
const fs = require('fs');
// const configFile = path.join(process.cwd(), './config.json');
const {app} = require('electron');
const AppDirectory = require('appdirectory');
const log = require('./log');
const mkdirp = require('mkdirp');

let dirs = new AppDirectory({
    appName: app.getName(), // the app's name, kinda self-explanatory
    appAuthor: "Jiejie.io", // The author's name, or (more likely) the name of the company/organisation producing this software.
                           // Only used on Windows, if omitted will default to appName.
    // appVersion: app.getVersion(), // The version, will be appended to certain dirs to allow for distinction between versions.
                         // If it isn't present, no version parameter will appear in the paths
    useRoaming: true, // Should AppDirectory use Window's roaming directories?  (Defaults to false)
    platform: process.platform // You should almost never need to use this, it will be automatically determined
})

const configFile = path.join(dirs.userData(), './config.json');

module.exports = function(config) {
	try {
		let data = JSON.stringify(config, null, 2);

		log.info('write config file', configFile, config);
		mkdirp.sync(path.dirname(configFile))

		fs.writeFileSync(configFile, data, 'utf8');
	} catch(err) {
		console.error(err, err.stack);
	}
}