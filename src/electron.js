const electron = require('electron');
const platform = require('os').platform();
// Module to control application life.
const {app, Menu, Tray, Notification, dialog, ipcMain} = electron;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;
const log = require('./log');
// const {autoUpdater} = require("electron-updater");

const path = require('path');
const url = require('url');
const argv = require('optimist').argv;

const server = require('./server');
const sync = require('./sync');
const config = require('./config')();
const configSave = require('./configSave');
const {printTemplate, getPrinters, defaultPrinter, getJob} = require('./print');
const PrinterManager = require('./print/manager');
const isDev = require('electron-is-dev');

global.config = config;
// require('./notification');

let tray = null
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let _force = null;


// autoUpdater.logger = log;
// autoUpdater.logger.transports.file.level = 'info';
log.info('App starting...');

function sendStatusToWindow(text) {
  log.info(text);
  mainWindow.webContents.send('message', text);
}

function createWindow() {
    // Create the browser window.
    mainWindow = new BrowserWindow({width: 800, height: 600});

    // and load the index.html of the app.
    const startUrl = process.env.ELECTRON_START_URL || url.format({
            pathname: path.join(__dirname, '/../build/index.html'),
            protocol: 'file:',
            slashes: true
        });
    mainWindow.loadURL(startUrl);
    // Open the DevTools.
    // mainWindow.webContents.openDevTools();
    mainWindow.on('close', function(event) {
        if (!_force) {
            event.preventDefault();
            mainWindow.hide();
        }
    })
    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    })

    mainWindow.forceClose = function() {
        _force = true;
        mainWindow.close();
    }

    app.mainWindow = mainWindow;
}

function settingWindow() {
    let child = new BrowserWindow({parent: mainWindow, modal: true, show: false, width: 400})
    child.loadURL(url.format({
        pathname: path.join(__dirname, '/../build/index.html'),
        protocol: 'file:',
        slashes: true
    }));
    // child.loadURL('https://github.com')
    child.once('ready-to-show', () => {
        child.webContents.executeJavaScript(() => location.href = '/settings')
        child.show()
    })
}

const isSecondInstance = app.makeSingleInstance((commandLine, workingDirectory) => {
  // Someone tried to run a second instance, we should focus our window.
  if (mainWindow) {
    if (mainWindow.isMinimized()) {
      mainWindow.restore()
      mainWindow.focus()
    }

    if (!mainWindow.isVisible()) {
      mainWindow.show()
      mainWindow.focus()
    }
  }
})

if (isSecondInstance) {
  app.quit()
}

app.on('before-quit', () => {
  _force = true
})

app.on('will-quit', () => {
    _force = true
})

log.info("electron argv", argv)
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);
if (!argv['disable-http']) {
  app.on('ready', server(app));
}
if (!argv['disable-sync']) {
  app.on('ready', sync(app));
}
app.on('ready', () => {

  let trayImage;  
  let imageFolder = __dirname + '/assets/images';

  // Determine appropriate icon for platform
  if (platform == 'darwin') {  
    trayImage = imageFolder + '/osx/trayTemplate.png';
  }
  else if (platform == 'win32') {  
    trayImage = imageFolder + '/win/tray.ico';
  }
  else if (platform == 'linux') {
    trayImage = imageFolder + '/osx/trayTemplate.png';

  }
  else {
    trayImage = imageFolder + '/osx/trayTemplate.png';
  }
  tray = new Tray(trayImage);

  // if (platform == "darwin") {  
  //   tray.setPressedImage(imageFolder + '/osx/tryHighlight.png');
  // }

  let items = [
    {label: '打开', click: ()=> mainWindow.show() },
    {label: '设置', click: ()=> settingWindow() },
    {label: '结束', click: ()=> { mainWindow.forceClose(); app.quit(); }},
  ];

  if (Notification.isSupported()) {
    let myNotification = new Notification({
      title: 'Title',
      body: 'Lorem Ipsum Dolor Sit Amet'
    })

    items.push({
      label: '-', type: 'separator'
    });
    items.push({
      label: '通知', click: () => myNotification.show()
    });

    items.push({
      label: 'Debug', click: () => mainWindow.webContents.openDevTools()
    });
    // items.push({
    //   label: '更新' + app.getVersion(), click: () => {
    //     log.info('start autoUpdate');
    //     autoUpdater.checkForUpdates()
    //   }
    // });    
    myNotification.onclick = () => {
      console.log('Notification clicked')
    };
  }
  const contextMenu = Menu.buildFromTemplate(items)

  tray.setToolTip('数据同步器')
  tray.setContextMenu(contextMenu)
  tray.on('double-click', () => {
    mainWindow.show();
  })
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

ipcMain.on('config:save', function(event, config) {
  configSave(config);
});

ipcMain.on('order:print', function(event, order) {
  let {defaultPrinter} = config;
  defaultPrinter = defaultPrinter || {};
  log.info('print order on printer', defaultPrinter.name, order)
  let corpOpts = {
    corpname: '耒阳市方盈商贸有限责任公司', 
    corpaddress: '耒阳市文化路（环保局对面) 4331139'
  }

  let printArg = {
    title: '耒阳市方盈商贸销售单', 
    order: order,
  }
  printArg = Object.assign(printArg, corpOpts)

  let printOpt = {
    show: false,
    silent: true,
    printerName: defaultPrinter.name,
    marginsType: 1,
    // pageSize: {
    //   width: 210000,
    //   height: 140000
    // },
    // mediaSize: {
    //     name: 'CUSTOM',
    //     custom_display_name: 'Custom',
    //     height_microns: 140000,
    //     width_microns: 210000
    //   }
    // format: 'A4' /*, orientation: 'landscape'*/
  }

  let tplName = 'test';

  printTemplate(tplName, printArg, printOpt).then(jobId => {
    log.info('jobId', jobId, defaultPrinter);
    let ret = {
      order: order,
      job: getJob(defaultPrinter.name, jobId)
    }

    log.info('return jobId', jobId, ret)
    event.sender.send('order:print:reply', ret)
    PrinterManager.push(jobId, {printerName: defaultPrinter.name});
  }).catch(err => {
    let ret = {
      order: order,
      error: {
        message: err.message,
        stack: err.stack
      }
    }
    log.info('return error', err, ret)
    event.sender.send('order:print:reply', ret)
  })
})

ipcMain.on('printers:get', function(event) {
  event.sender.send('printers:all', getPrinters())
})

ipcMain.on('printer:get_default', function(event) {
  event.sender.send('printer:default', defaultPrinter())
})

ipcMain.on('printer:selected', function(event, printer) {
  config.defaultPrinter = {name: printer.name}
  configSave(config)
  event.returnValue = true
})

PrinterManager.on('order:print:status', job => (
  mainWindow.webContents.send('order:print:status', job)
))

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
