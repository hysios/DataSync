const path = require('path');
const fs = require('fs');
const ejs = require('ejs')
const tmp = require('tmp')
// const options = { format: 'Letter' };

const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const log = require('./log');
const errorHandler = require('errorhandler')
const morgan = require('morgan')
const sequelizePagination = require('sequelize-paginate-cursor').default;
const {generateHTML} = require('./print');

const {
	Received,
	ReceivedDetail, 
	Invoice, 
	Inout, 
	SaleDetail, 
	Client, 
	Good, 
	GoodType,
	GoodUnit, 
	Store,
	Message,
	Employ,
	KeyTable,
	sequelize
} = require('./models');

sequelizePagination(Message, { name: 'paged' }); // custom name, default 'paginate'

module.exports = function(electronApp) {
return function() {
	app.use(morgan('tiny'))
	app.use(cors())
	app.use(bodyParser.urlencoded({
	  extended: true
	}));
	app.use(bodyParser.json());
	app.get('/', (req, res) => {
		res.send('hello world');
	});

	app.post('/products', function(req, res, next) {
		let {offset, limit} = req.body;
		if (!offset) offset = 0;
		if (!limit) limit = 100;

		Good.findAndCountAll({offset, limit}).then(results => {
			res.send(results)
		}).catch((err) => {
			log.error('products', err)
			next(err)
		}) 		
	});

	app.post('/customers', function(req, res, next) {
		let {offset, limit} = req.body;
		if (!offset) offset = 0;
		if (!limit) limit = 100;

		Client.findAndCountAll({offset, limit}).then(results => {
			res.send(results)
		}).catch((err) => {
			log.error('customers', err)
			next(err)
		}) 		
	});

	app.post('/customers/create', function(req, res, next) {
		let {body} = req.body;
		log.info('create customer', body)
		Promise.all([Client.lastID(), Client.lastCode()]).then(results => {
			let ID = results[0], code = results[1];
			let attrs = Object.assign({clientid: ID, code}, body);

			if (attrs['isdouble']) {
				attrs['isdouble'] = 'T'
			} else {
				attrs['isdouble'] = 'F'
			}

			log.info('create customer with attrs', attrs)
			return Client.findOrCreate({where: {shortname: attrs['shortname']}, defaults: attrs})
		}).then((results) => {
			log.info('create customer', results[0])

			res.send(results[0])
			return results[1]
		}).then(created => {
			if (created) {
				return KeyTable.increment('client')
			}
		}).catch(err => {
			log.error('create customer error', err, err.stack)
			next(err)
		})
	})

	app.post('/employee', function(req, res, next) {
		let {offset, limit} = req.body;
		if (!offset) offset = 0;
		if (!limit) limit = 100;

		Employ.findAndCountAll({offset, limit}).then(results => {
			res.send(results)
		}).catch((err) => {
			log.error('customers', err)
			next(err)
		}) 		
	});

	app.post('/employee/create', function(req, res, next) {
		let {body} = req.body;
		log.info('create employee', body)
		Employ.all([Employ.lastID(), Employ.lastCode()]).then(results => {
			let ID = results[0], empcode = results[1];
			let attrs = Object.assign({empid: ID, empcode}, body);
			log.info('create employee with attrs', attrs)
			return Employ.create(attrs)
		}).then(employ => {
			log.info('create employee', employ)
			res.send(employ)
			return KeyTable.increment('employ')
		}).catch(err => {
			log.error(err, err.stack)
			next(err)
		})
	})

	app.post('/goodsunits', function(req, res, next) {
		let {offset, limit} = req.body;
		if (!offset) offset = 0;
		if (!limit) limit = 100;

		GoodUnit.findAndCountAll({offset, limit}).then(results => {
			res.send(results)
		}).catch((err) => {
			log.error('goodsunits', err)
			next(err)
		}) 		
	})

	app.post('/purchase', function (req, res, next) {
		let {offset, limit, order} = req.body,
			includeOpts = [
				{model: Client, attributes: ["shortname"]}
			];
		if (!order) {
			order = [['billdate', 'DESC']];
		}
		Received.findAndCountAll({offset, limit, order, include: includeOpts}).then(results => {
			res.send(results)
		}).catch(next) 
	});
	
	app.post('/purchase/:code', function (req, res, next) {
		let {offset, limit} = req.body;

		Received.findOne({
			where: {code: req.params.code},
			include: [
				{model: Client, attributes: ['shortname']},
				{model: Store, attributes: ['name']}
			]
		}).then(async result => {
			let details = await ReceivedDetail.findAndCountAll({
				where: {recievedid: result.recievedid},
				offset,
				limit,				
				include: [
					{model: Good},
					{model: GoodUnit}
				]
			});

			let quantityTotal = await ReceivedDetail.sum('quantity', {where: {recievedid: result.recievedid} }),
				amountTotal = await ReceivedDetail.sum('amount', {where: {recievedid: result.recievedid} });
			details.quantityTotal = quantityTotal;
			details.amountTotal = amountTotal;
			let json = result.toJSON();
			json['receivedDetails'] = details;

			res.send(json)
		}).catch(next)
	});

	app.post('/sales', function(req, res, next) {
		let {offset, limit, order} = req.body,
			includeOpts = [
				{model: Client, attributes: ["shortname"]}
			];
		if (!order) {
			order = [['billdate', 'DESC'], ['code', 'DESC']];
		}
		Invoice.findAndCountAll({offset, limit, order, include: includeOpts}).then(results => {
			res.send(results)
		}).catch(next) 		
	})

	app.post('/sales/:code', function(req, res, next) {
		let {offset, limit} = req.body;

		Invoice.findOne({
			where: {code: req.params.code}, 
			include: [
				{model: Client, attributes: ['shortname']},
				{model: Store, attributes: ['name']},
				{model: Employ, attributes: ['name', 'telephone', 'mobilephone'], as: 'employ'}
			]
		}).then(async result => {
			console.log(result)
			let details = await SaleDetail.findAndCountAll({
				where: {invoiceid: result.invoiceid},
				offset,
				limit,				
				include: [
					{model: Good},
					{model: GoodUnit},
				]
			});

			let quantityTotal = await SaleDetail.sum('unitqty', {where: {invoiceid: result.invoiceid} }),
				amountTotal = await SaleDetail.sum('amount', {where: {invoiceid: result.invoiceid} });
			details.quantityTotal = quantityTotal;
			details.amountTotal = amountTotal;

			let json = result.toJSON();
			json['saleDetails'] = details;
			res.send(json)
		}).catch(next)	
	})

	app.post('/inventory', async function(req, res, next) {
		let {offset, limit, noalloc} = req.body, count, sql_where = '';
		limit = limit ? limit: 10;
		offset = offset ? offset: 0;
		if (limit > 1000) {
			limit = 1000
		}

		if (noalloc) {
			sql_where = `WHERE a.billname != '库存变动单'`;
		} 

		let sql_orgin = `
			SELECT CASE WHEN billname='库存初始化' THEN base_inqty ELSE 0 END as init_qty, * FROM dbo.inv_inout a
		`

		let sqlinv = `
			SELECT 
				goodsid,
				max(store_name) as store_name, 
				max(goods_code) as goods_code, 
				max(goods_name) as goods_name, 
				max(base_unitname) as base_unitname, 
				sum(init_qty) as init_qty,
				sum(base_inqty) as base_inqty, 
				sum(base_outqty) as base_outqty,
				sum(inamt) as inamt, 
				sum(bill_inqty) as bill_inqty,
				sum(bill_outqty) as bill_outqty,
				sum(outamt) as outamt,  
				(sum(base_inqty)-sum(base_outqty)) as qty,
				(sum(inamt)-sum(outamt)) as amount
			FROM (${sql_orgin}) b ${sql_where} GROUP BY b.goodsid		
		`
		let sqlinv_with_rownumber = `
			SELECT 
				ROW_NUMBER() OVER (order by c.goods_code) as row_num, * 
			FROM (${sqlinv}) c
		`

		let sqlinv_with_offsetlimit = `
			SELECT TOP ${limit}
				goodsid, 
				store_name, 
				goods_code,
				goods_name,
				base_unitname,
				init_qty,
				(base_inqty - init_qty) as base_inqty,
				inamt,
				base_outqty,
				bill_outqty,
				bill_inqty,
				outamt,
				qty,
				amount FROM (${sqlinv_with_rownumber}) d WHERE d.row_num > ${offset}
		`

		let sqlinv_count = `
			SELECT count(*) as count FROM (${sqlinv}) d
		`

		let allInventoryCount = new Promise((resolve, reject) => {
			sequelize.query(sqlinv_count).then((result) => {
				resolve(result[0].count)
			}).catch(reject)
		});
		let pageInventory = new Promise((resolve, reject) => {
			sequelize.query(sqlinv_with_offsetlimit).then(result => {
				resolve(result)
			}).catch(reject)
		});

		Promise.all([allInventoryCount, pageInventory])
			.then(results => {
				let count = results[0], rows = results[1];
				res.send({
					count,
					rows
				});
			}).catch(next);
	})

	app.post('/open', function(req, res) {
		if (electronApp) {
			electronApp.mainWindow.show();
		}
		res.send('success');
	})

	app.post('/messages', function(req, res, next) {
		let {sinceId, limit} = req.body;
		if (!limit) {
			limit = 10
		}
		Message.paged({
			sinceId,
			limit
		}).then(result => {

			res.send(result)
		}).catch(next)			
		// res.send(attributes);

	})

	app.post('/messages/create', function(req, res, next) {

		Message.create(req.body).then(results => {
			res.send(results)
		}).catch(next)			
	})

	app.post('/printer', function(req, res, next) {
		let {tplName, context} = req.body;
		log.info('call printer service', tplName, context)

		try {
			res.send(generateHTML(tplName, context))
		} catch(err) {
			next(err)
		}
	});

	app.use(errorHandler());

	Promise.all([
		createMessageTable(),
		fixedLastID()
	])

	app.listen(3001, () => {
		console.log('DataSync Server listen on port 3001')
	});
};
}


function attrName(attr) {
	let prefix = `${attr.fieldName} ${attr.type.toString()}`
	prefix += attr.primaryKey || attr.allowNull === false ? ' NOT NULL' : ' NULL'
	prefix += attr.fieldName == 'id' ? ' identity(1, 1)': '';
	return prefix;
}

function createMessageTable() {
	let { tableName, tableAttributes } = Message;
	let attributes = Object.keys(tableAttributes).map((field) => {
		return attrName(tableAttributes[field])
	})

    // ALTER TABLE ${tableName} ADD CONSTRAINT pk_${tableName} PRIMARY KEY (messageId);

	return sequelize.query(`if not exists (select * from sysobjects where name='${tableName}' and xtype='U')
		BEGIN
	    CREATE TABLE ${tableName} (
	    	${attributes.join(',')}
	    );
	    ALTER TABLE ${tableName} ADD CONSTRAINT pk_${tableName} PRIMARY KEY (id);
		END`).spread((result, meta) => {})

}

function realityLastID(model, idcolumn) {
	return new Promise((resolve, reject) => {
		log.info('model', model)
		model.findOne({order: [[idcolumn, 'desc']]}).then(obj => {
			resolve(obj ? obj[idcolumn] : 1)
		}).catch(reject)
	})
}

function fixedLastID() {
	Promise.all([
		realityLastID(Client, 'clientid'),
		realityLastID(Employ, 'empid'),
		realityLastID(Good, 'goodsid'),
		realityLastID(GoodType, 'goodstypeid'),
		realityLastID(Invoice, 'invoiceid'),
		realityLastID(Received, 'recievedid')
	]).then(results => {
		log.info('reality lastIDs', results)
		return Promise.all([
			KeyTable.setKey('client', results[0]),
			KeyTable.setKey('employ', results[1]),
			KeyTable.setKey('goods', results[2]),
			KeyTable.setKey('goodstype', results[3]),
			KeyTable.setKey('invoice', results[4]),
			KeyTable.setKey('recieved', results[5]),
		])
	}).catch(err => {
		log.error('fixed lastID', err)
	})
	
}
