// const config = require('config');
const Table = require('cli-table-cjk');

// function connectionString() {
// 	let dbconfig = config.database;
// 	let {dialect, dbname, username, password, host} = dbconfig;
// 	return `${dialect}://${username}:${password}@${host}/${dbname}`;
// }

/**
 * printTable 打印表格，讲一个数组记录，以表格方式打印出来。
 * @param  {[type]} rows [description]
 * @param  {Array}  cols [description]
 * @return {[type]}      [description]
 */
function printTable(rows, cols = []) {
	let {titles, columns, formats}= parseColumns(cols),
		table = new Table({
			head: titles,
		});

	function parseColumns(cols) {
		let _titles = [], _columns = [], _formats = [];
		for (let i in cols) {
			let col = cols[i],
				t = typeof col;

			switch (t) {
			case 'string':
				_titles.push(col);
				_columns.push(col);
				_formats.push(null);
				break;
			case 'object': 
				_titles.push(col.title || col.column);
				_columns.push(col.column);
				_formats.push(col.format || null);
				break;
			}
		}

		return {
			titles: _titles,
			columns: _columns,
			formats: _formats
		}
	}

	function get(obj, name) {
		if (typeof obj.get === 'function') {
			return obj.get(name);
		} else {
			return obj[name];
		}
	}

	for (let i in rows) {
		let row = rows[i],
			line = [];

		for (let j in columns) {
			let col = columns[j],
				sp = col.split('.'),
				val = null,
				fmt = formats[j];
			if (sp.length > 1) {
				val = sp.reduce((s, m) => {
					return get(s, m);
				}, row)
			} else {
				val = get(row, col)
			}

			if (fmt && typeof fmt === "string") {
				switch(fmt) {
				case "iso":
					val = val["toISOString"].apply(val)
				}
			} else if (fmt && typeof fmt === "function") {
				val = fmt(val)
			}
			line.push(val || "");
		}
		table.push(line);
	}
	console.log(table.toString())
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function succ(input) {
  var alphabet = 'abcdefghijklmnopqrstuvwxyz',
    length = alphabet.length,
    result = input,
    i = input.length;

  while(i >= 0) {
    var last = input.charAt(--i),
        next = '',
        carry = false;

    if (isNaN(last)) {
        index = alphabet.indexOf(last.toLowerCase());

        if (index === -1) {
            next = last;
            carry = true;
        }
        else {
            var isUpperCase = last === last.toUpperCase();
            next = alphabet.charAt((index + 1) % length);
            if (isUpperCase) {
                next = next.toUpperCase();
            }

            carry = index + 1 >= length;
            if (carry && i === 0) {
                var added = isUpperCase ? 'A' : 'a';
                result = added + next + result.slice(1);
                break;
            }
        }
    }
    else {
        next = +last + 1;
        if(next > 9) {
            next = 0;
            carry = true
        }

        if (carry && i === 0) {
            result = '1' + next + result.slice(1);
            break;
        }
    }

    result = result.slice(0, i) + next + result.slice(i + 1);
    if (!carry) {
        break;
    }
  }
  return result;
}

function delay(duration) {
	return new Promise((resolve) => {
		setTimeout(resolve, duration);
	})
}

function generateUUID() {
	function S4() {
		return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
	}
	return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4());
}

module.exports = {
	// connectionString,
	isEmpty,
	printTable,
	succ,
	delay,
	generateUUID
}

