const log = require('electron-log');
log.transports.file.level = 'info';

module.exports = log;