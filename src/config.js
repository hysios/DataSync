
const path = require('path');
const fs = require('fs');
// const configFile = path.join(process.cwd(), './config.json');
const {app} = require('electron');
const AppDirectory = require('appdirectory')
const log = require('./log');

let dirs = new AppDirectory({
    appName: app ? app.getName(): "DataSync", // the app's name, kinda self-explanatory
    appAuthor: "Jiejie.io", // The author's name, or (more likely) the name of the company/organisation producing this software.
                           // Only used on Windows, if omitted will default to appName.
    // appVersion: app.getVersion(), // The version, will be appended to certain dirs to allow for distinction between versions.
                         // If it isn't present, no version parameter will appear in the paths
    useRoaming: true, // Should AppDirectory use Window's roaming directories?  (Defaults to false)
    platform: process.platform // You should almost never need to use this, it will be automatically determined
})

const configFile = path.join(dirs.userData(), './config.json');
	// "host": "ec2-54-223-106-30.cn-north-1.compute.amazonaws.com.cn",
	// 	"username": "hysios",
	// 	"password": "Wocaonimade1",
	// 	// "dbname": "SD11831N_WeiXin",
	// 	"dbname": "SD11831N_20161101",
	// 	"dialect": "mssql"
	// 					
	// 	"host": "192.168.1.222",
	// "username": "hysios",
	// "password": "asdfasdf",
	// "dbname": "SD11831N_WeiXin",
	// // "dbname": "SD11831N_20161101",
	// "dialect": "mssql"
module.exports = function() {
	let config = {};

	try {
		log.info('config file', configFile);
		config = JSON.parse(fs.readFileSync(configFile, 'utf-8'));
		log.info('config:', config)
	} catch(err) {
		console.error(err, err.stack);
		config = {
			"database": {
				"host": "ec2-54-223-106-30.cn-north-1.compute.amazonaws.com.cn",
				"username": "hysios",
				"password": "Wocaonimade1",
				"dbname": "SD11831N_20161101",
				// "dbname": "SD11831N_20161101",
				"dialect": "mssql"
			}
		}
	}

	global.config = config;
	return global.config;
}