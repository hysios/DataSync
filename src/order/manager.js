const EventEmitter = require('events');
const util = require('util');
const {Notification} = require('electron');

const {
  Client,
  Employ,
  Good,
  GoodUnit,
  Invoice,
  Message,
  Operator,
  SaleDetail,
  KeyTable,
  Onhand,
  DetailBillFlow,
  sequelize,
} = require('../models');
const {
	ITEM_MAX_DISPLAY, 
	GIFT_MAX_DISPLAY,
	EMPTY
} = require('../constants');
const { delay, isEmpty } = require('../utils');
const { getAttr } = require('../queue/utils');
const errors = require('../errors');
const log = require('../log');

class OrderManager extends EventEmitter {
	constructor(options = {}) {
		super()
		this.app = options.app;
		this._queue = [];
	}

	async run() {
		if (this._running) {
			return
		}
		this._running = true

		while (1) {
			log.info('queue', this._queue)

			let raw = this._queue.shift();
			if (!raw) {
				break;
			}
			let {msg, meta} = raw;
			log.info('msg', msg)
			if (!msg) {
				break;
			}

			log.info('pull message')
			try {
				let order = await this.createOrder(msg, meta)
				msg.reply({
					order: order
				})
			} catch (err) {
				msg.reply({
					error: err
				})
			}
		}
	
		this._running = false
	}

	push(msg, meta) {
		log.info('push queue', msg)
		this._queue.push({msg, meta})
		this.run()
	}

	createOrder(msg, meta) {
		return new Promise((doneResolve, doneReject) => {
			let baseMsg = {
					messageId:  getAttr(meta, 'MsgID'),
					type: 		getAttr(meta, 'Type'),
					command: 	getAttr(meta, 'Command'),
					msgPart: 	getAttr(meta, 'MsgPart'),
					msgCount: 	getAttr(meta, 'MsgPartCount'),
					status: 	'new',
					returnURL: 	getAttr(meta, 'ReturnURL'),
					body: 		msg.getData(),
				};
			try {
				let body = JSON.parse(baseMsg.body);

				if (isEmpty(body.customerName)) {
					let err = {
						customer: {
							code: errors.MISSING_CUSTOMER,
							message: 'missing customerName'
						}
					};
					log.error(err)
					msg.reply({error: err})
					return 
				}

				if (body.items && body.items.length === 0) {
					let err = { 
						general: {
							code: errors.EMPTY_ITEMS,
							message: 'items must not empty'
						}
					};

					log.error(err)
					msg.reply({error: err})
					return 
				}			
				// log.info('baseMsg', baseMsg);
					
				let getGoodUnits = (items) => {
					return new Promise((resolve) => {
						Good.findAll({where: {
							name: items.map(({goodName}) => goodName) 
						}}).then(goods => {
							return Promise.all(goods.map(good => good.getGoodUnits()))
						}).then(resolve)
					})
				}

				// 第一阶段获取, 客户, 操作员, 业务员, 产品等信息, 
				Message.create(baseMsg).then((message) => {
					log.info('create message', message)

					let context = {
						message: message
					}
					return context;
				// }).then(context => {
				// 	let {items, gifts} = body;
				// 	items = items.concat(gifts);
				// 	let missingBarcode = items.filter(({barcode}) => isEmpty(barcode))
				// 	if (missingBarcode.length > 0) {
				// 		throw {
				// 			items: missingBarcode.map((item) => ({
				// 				id: item.itemId,
				// 				code: errors.MISSING_BARCODE,
				// 				message: item.productName + " missing barcode"
				// 			}))
				// 		}
				// 	} else {
				// 		return context
				// 	}
				}).then(context => {
					let {address, salerName, customerName, storehouseId, total, items, gifts} = body;
					let weixinId = 1;
					items = items.concat(gifts);
					let unitAttrs = ['unitid', 'goodsid', 'name', 'rate', 'barcode', 'unitname', 'pricetype', 'sprice', 'pprice']
					
					return new Promise((resolve, reject) => {
						Promise.all([
							Client.find({where: {shortname: customerName}}),
							Operator.find({where: {opid: weixinId}}),
							Employ.find({where: {name: salerName}}),
							Good.findAll({where: { 
								name: items.map(({goodName}) => goodName)
							}, include: [{model: GoodUnit, as: 'goodUnits' }]}),
							// GoodUnit.findAll({where: {
							// 	barcode: items.map(({barcode}) => barcode)
							// }, include: [{model: Good, as: 'good' }]}),
							// Onhand.findAll({where: {
							// 	storeid: storehouseId || 1, 
							// 	goodsid: items.map(({goodsId}) => goodsId)
							// }})
						]).then(results => {
							context.results = results
							resolve(context)
						}).catch(reject)
					})
				}).then(context => {
					let {results} = context
					if (!results[0]) {
						throw {
							customer: {
								code: errors.NOT_FOUND_CUSTOMER,
								message: "can't found this customer"
							}
						}
					}
					log.info('goods', results[3])

					context = Object.assign(context, {
						client: results[0],
						operator: results[1],
						employ: results[2],
						goods: results[3],
						// units: units
						// onhand: results[5]
					});

					return context;
				}).then(context => {
					return new Promise((resolve, reject) => {
						Invoice.lastCode().then(code => {
							context.lastCode = code
							resolve(context)
						}).catch(reject)
					})
				// 五, 生成表单属性在 context 
				}).then(context => {

					let {lastCode} = context;
					let {clientid} = context.client;
					let {address, storehouseId, total} = body;
					let {empid} = context.employ || {};
					let weixinId = 1;
					let defautlStoreId  = 1;
					let defaultAttrs = {
						billtypeid: 1,
						shipto: address,
						storeid: storehouseId || defautlStoreId,
						shopid: 0,
						templateid: 2,
						opid: weixinId,	
						exemanid: (typeof empid === 'undefined') || empid == 0 ? null : empid,
						totalamt: total
						// exemanid: 
					};
					let now = new Date();
					let attrs = Object.assign(defaultAttrs, {
						code: lastCode,
						clientid: clientid,
						billdate: now.toISOString().slice(0, 10)
					})
					log.info('create attrs')
					context.attrs = attrs;
					return context
				// 六, 生成销售单的 ID
				}).then(context => {
					let {attrs} = context;
					return new Promise((resolve, reject) => {
						Invoice.lastID().then(ID => {
							log.info('invoice last ID', ID)
							attrs.invoiceid = ID
							resolve(context)
						}).catch(reject)
					})
				// 七, 创建销售单的原信息.
				}).then(context => {
					let {attrs} = context;
					log.info('create order', attrs);

					let attrNames = Object.keys(attrs).map(name => '['+ name + ']').join(',');
					let attrValues = Object.keys(attrs).map(name => util.inspect(attrs[name])).join(',');
					let sql = `
						INSERT INTO invoice (${attrNames}) 
						VALUES (${attrValues});
						SELECT * FROM invoice WHERE [invoiceid] = ${attrs.invoiceid};
					`
					return new Promise((resolve, reject) => {
						
						sequelize.query(sql, {type: sequelize.QueryTypes.SELECT}).then(result => {
							context.order = result[0];
							resolve(context)
						}).catch(reject)
					})
					// return Invoice.spread(Object.assign(defaultAttrs, {
					// 	code: code,
					// 	billdate: now.toISOString().slice(0, 10)
					// }))
				// 创建销售清单数据
				}).then(context => {
					return new Promise((resolve, reject) => {
						KeyTable.increment('invoice').then(() => {
							resolve(context)	
						}).catch(reject)
					});
				}).then(context => {
					let {/*units*/ attrs, goods} = context;
					// let goodUnities = {};

					// units.forEach(unit => {
					// 	goodUnities[unit.unitname] = unit;
					// })	

					// let goodInfos = {}
					// units.forEach(unit => {
					// 	goodInfos[unit.unitname] = unit.good
					// });

					// log.info('goodUnities', goodUnities);
					let giftsIndex = body.items.length + 1;

					let getGoodUnit = (goodName, unitName) => {
						log.info('goodname', goodName, 'unitName', unitName)
						let good = getGood(goodName)
						if (!good) {
							return null;
						}

						log.info('goodUnits', good.goodUnits);


						let unit = good.goodUnits.find(({unitname}) => unitname == unitName);
						return unit ? unit : good.goodUnits[0];
					}

					let getGood = (goodname) => {
						return goods.find(({name}) => name == goodname);
					}

					let buildItem = (item, index) => {
						let {barcode, quantity, unitName, price} = item;
						// let unit = goodUnities[barcode];

						let unit = getGoodUnit(item.goodName, unitName)
						if (!unit) {
							let err = {
								items: [
									{
										id: item.itemId,
										code: errors.DONT_HAVE_UNITNAME,
										value: unitName,
										message: item.productName + " goods don't have this unit " + unitName
									}
								]
							}
							// log.error(err)
							throw err
						}
						// let good = goodInfos[barcode];
						let good = getGood(item.goodName);
						price = (typeof price == 'undefined') ? unit.sprice : price;
						let amount = price * quantity;
						let base_quantity = quantity * unit.rate;
						let disc = 100
						return {
							invoiceid: attrs.invoiceid,
							itemno: index + 1,
							goodsid: good.goodsid,
							unitid: unit.unitid,
							aprice: good.aprice,
							disc: disc,
							price: amount / base_quantity,
							unitprice: price,
							unitqty: quantity,
							pricetype: unit.pricetype,
							quantity: base_quantity,
							taxrate: 0,
							taxamt: 0,
							ispresent: false,
							returnqty: 0,
							refercount: 0,
							scorerate: 1,
							goodsamt: price * quantity,
							taxamt: 0,
							amount: price * quantity * disc / 100
						}
					}

					let pureGift = (item, i) => {
						return Object.assign(item, {itemno: giftsIndex + i, price: 0, unitprice: 0, amount: 0, nprice: 0, memo: '礼品'})
					}

					let items = body.items.map(buildItem)
					let gifts = body.gifts.map(buildItem).map(pureGift)
					items = items.concat(gifts)

					return new Promise((resolve, reject) => {
					 	SaleDetail.bulkCreate(items).then(items => {
					 		context.items = items
					 		resolve(context)
					 	}).catch(reject)
					})
				}).then(context => {
					return new Promise((resolve, reject) => {
						DetailBillFlow.lastID().then(lastID => {
							log.info('DetailBillFlow last ID', lastID)
							context.billflowID = lastID
							resolve(context)
						}).catch(reject)
					})
				}).then(context => {
					let {items, lastCode, billflowID, attrs } = context;
					let flowattrs = items.map((item, idx) => {
						return {
							// id: billflowID + idx,
							billdate: new Date,
							billtype: 3,
							billid: item.invoiceid,	
							itemno: item.itemno,
							billcode: lastCode,
							storeid: attrs.storeid,
							goodsid: item.goodsid,
							unitid: item.unitid,
							unitprice: item.unitprice,
							unitqty: item.unitqty,
							inqty: 0,
							inprice: 0,
							inamt: 0,
							outqty: item.quantity,
							outprice: item.aprice,
							outamt: item.quantity * item.aprice,
							eqty: 0,
							eprice: 0,
							eamount: 0,
							difamount: 0,
							batchcode: null,
							produceddate: null,
							validdate: null,
							batchrefid: null,
							caculateflag: false,
							needupdatebill: false,
							locked: false
						}
					})
					return new Promise((resolve, reject) => {
						DetailBillFlow.bulkCreate(flowattrs).then(billflows => {
							context.billflows = billflows
							resolve(context)
						}).catch(reject)
					})
				}).then(context => {
					let {message} = context;
					return new Promise((resolve, reject) => {
						message.status = 'saved'
						message.save().then(() => {
							resolve(context)
						}).catch(reject)
					})
				}).then(context => {
					let {order} = context
					// msg.reply(order)
					if (this.app && this.app.mainWindow) {
						log.info('post message to mainWindow');
						let order = JSON.parse(msg.getData());
						log.info('post order', order, orderFormat(order));
						this.app.mainWindow.webContents.send('order:create', {id: order.orderId, order: order});
						let notify = new Notification({
							title: '新的订单到来',
							body: orderFormat(order)
						});
						notify.show();
						notify.on('click', () => {
							this.app.mainWindow.show();
						})
					}
					doneResolve(order)
				}).catch(err => {
					log.error(typeof err, err)
					if (util.isError(err)) {
						// msg.reply({error: })
						err = {
							general: {
								message: err.message
							}
						};
					}
					doneReject(err)
				})
			} catch(err) {
				doneReject({general: {message: err.message}})
			}
		})
	}
}

let _manager
module.exports = function(options) {
	if (!_manager) {
		_manager = new OrderManager(options)
	}
	return _manager
}

function orderFormat(order) {
	let items = order.items.map(item => (
		item.productName
	))

	let gifts = order.gifts.map(gift => (
		gift.productName
	))

	let itemNames = items.slice(0, ITEM_MAX_DISPLAY).join(',') + (items.length > ITEM_MAX_DISPLAY ? '等': EMPTY);
	let giftNames = gifts.slice(0, GIFT_MAX_DISPLAY).join(',') + (gifts.length > GIFT_MAX_DISPLAY ? '等': EMPTY);
	log.info('item', items, itemNames)
	let prefix = `${order.customerName}需要: ${itemNames}`
	if (gifts.length > 0) {
		prefix += `及礼品:${giftNames}`
	}
	let unit = order.unit ? order.unit : '提';
	if (order.total > 0) {
		prefix += `共计${order.total}${unit}货`
	}
	if (order.deliveryTime) {
		prefix += `在${order.deliveryTime}前`
	}

	prefix += `送到${order.address}`
	return prefix;
}
