const GraphClient = require('graphql-client');
const EventEmitter = require('events');
const util = require('util');

class Remote extends EventEmitter {

	constructor(options = {}) {
		super()
		let defaultURL = 'http://jiejie.wanliu.biz/graphql';
		let {url, token} = Object.assign(options, {url: defaultURL})

		this.client = GraphClient({
			url, 
			headers: {
				Authorization: 'Bearer ' + token
			}
		})
	}

	products(options = {}) {
		let {name, cursor, limit, source} = Object.assign(options, {limit: 10, source: true});
		let table = source ? 'sourceProducts': 'products';
		let ql = `
		  query searchProductsQuery($name: String, $cursor: String, $limit: Int!) {
		  	viewer {
			  ${table}(name: $name, after: $cursor, first: $limit) {
			    edges {
			      node {
			        id
			        name
			        sourceJson
					productUnits {
			            name
			            price
			            mutiplier
			            barcode
			            isBase
			            pprice
			            productId
			            isDefault
			            sourceJson
			        }
			        updatedAt
			        createdAt
			      }
			    }
			    pageInfo {
			    	endCursor
			    	hasNextPage
			    	hasPreviousPage
			    }
			  }
			}
		  }
		`,
		variables = `{
			"clientMutationId": 0,
			"name": "${name || ''}",
			"limit": ${limit},
			"cursor": "${cursor || ''}"
		}`
		// console.log(ql, variables)
		return this.client.query(ql, variables, (req, res) => {
			if(res.status === 401) {
				throw new Error('Not authorized')
			}
		}).then(body => {
			let viewer = body['data']['viewer'];
			return viewer[table]
		})
	}

	customers(options = {}) {
		let {name, cursor, limit, source} = Object.assign(options, {limit: 10, source: true});
		let table = source ? 'sourcePeople': 'people';
		let ql = `
		  query searchCustomerQuery($name: String, $cursor: String, $limit: Int!) {
			viewer {
				${table}(name: $name, after: $cursor, first: $limit) {
			    edges {
			        node {
			          id
			          name
			          sourceJson
			        }
			    }
			    pageInfo {
			    	endCursor
			    	hasNextPage
			    	hasPreviousPage
			    }
			  }
			}
		  }
		`,
		variables = `{
			"clientMutationId": 0,
			"name": "${name || ''}",
			"limit": ${limit},
			"cursor": "${cursor || ''}"
		}`

		return this.client.query(ql, variables, (req, res) => {
			if(res.status === 401) {
				throw new Error('Not authorized')
			}	
		}).then(body => {
			let viewer = body['data']['viewer'];
			return viewer[table]
		})
	}

	employees(options = {}) {
		let {name, cursor, limit, source} = Object.assign(options, {limit: 10});
		let table = 'sourceUsers';
		let ql = `
		  query searchEmployeeQuery($name: String, $cursor: String, $limit: Int!) {
		  	viewer {
				${table}(name: $name, after: $cursor, first: $limit) {
			    edges {
			        node {
						id
						name
						sourceJson
			        }
    			}
			    pageInfo {
			    	endCursor
			    	hasNextPage
			    	hasPreviousPage
			    }
			  }
			}
		  }
		`,
		variables = `{
			"clientMutationId": 0,
			"name": "${name || ''}",
			"limit": ${limit},
			"cursor": "${cursor || ''}"
		}`

		return this.client.query(ql, variables, (req, res) => {
			if(res.status === 401) {
				throw new Error('Not authorized')
			}	
		}).then(body => {
			let viewer = body['data']['viewer'];
			return viewer[table]
		})
	}
}


module.exports = Remote;