const path = require('path');
const Promise = require('bluebird');
const fs = require('fs');
const execFile = Promise.promisify(require('child_process').execFile);
const stat = Promise.promisify(fs.stat);
const download = require('download');
const AdmZip = require('adm-zip-electron');
const mkdirp = require('mkdirp');
// const printer = require('electron-printer');
const os = require('os');

// const Unzip = require('unzip');
// 
// var extract = require('extract-zip')

const log = require('../log');

let config = {
	bin: './bin',
	downloadURL: 'https://s3.cn-north-1.amazonaws.com.cn/jiejie-deploy/gsprint.zip'
}

function downloadZip(url, target)  {
	log.info('download file', url, ' to', target)
	return download(url, target)
}

function unzip(zipfile, target) {
	// let data = fs.readSyncFile(zipfile);
	return new Promise(resolve => {
		log.info('unzip', zipfile, ' to', target)
		let zip = new AdmZip(zipfile);
		zip.extractAllToAsync(target, true, (err) => {
			if (err) {
				reject(err)
				return
			}

			resolve(target)
		})
		// extract(zipfile, {dir: target}, function (err) {
		// 	if (err) {
		// 		reject(err)
		// 		return 
		// 	}
		// 	resolve(target)
		// })
	})
}

function mkdirBin(dir) {
	return new Promise((resolve, reject) => {
		mkdirp(dir, function (err) {
		    if (err) reject(err)
		    else resolve(dir)
		});
	})
}
 

function checkFileOrDownload(file) {
	return new Promise((resolve, reject) => {
		stat(file).then(stats => {
			resolve(file)
		}).catch(err => {
			if (err.code === 'ENOENT') {
				log.error('gsprint don\'t at the', file)
				let dest = path.join(process.cwd(), config.bin)
				mkdirBin(path.dirname(dest)).then(dir => {
					return downloadZip(config.downloadURL, dest)
				}).then(file => {
					return unzip(file, dest);
				}).then(() => {
					resolve(file);
				}).catch(err => {
					log.error(err, err.stack);
					reject(err)
				})

			}
		})	
	})
}

function gsprintFile(filename, printerName, options={}) {
	return new Promise((resolve, reject) => {
		if( process.platform !== 'win32') {
			throw Error('invalid os platform, must a windows system')
		}

		let name =  'gsprint-' + process.arch + '.exe'
		let file = path.join(process.cwd(), config.bin, name)

		let outputName = printerName
		log.info('gsprint:', file)

		checkFileOrDownload(file).then(() => {
			width = Math.ceil(24 * 0.393700787 * 72)
			height = Math.ceil(14 *  0.393700787 * 72)

			let printOpt = options['print'] || [
				'-printer',
				outputName,
				'-ghostscript',
				'-dDEVICEWIDTHPOINTS=' + width,
				'-dDEVICEHEIGHTPOINTS=' + height
			]

			log.info('print with options',  printOpt, filename)
			Promise.all([
				execFile(file, [...printOpt, filename]),
				getPrintJob(filename, printerName).then(resolve).catch(reject)
			]).then(results => {
				let output = results[0];
				let jobId = results[1];
				log.info('output', output)
				resolve(jobId)
			}).catch(reject)
		})
	})
}

function getPrintJob(filename, printerName) {
	return new Promise((resolve, reject) => {
		let checkId = setInterval(() => {
			// let print = printer.getPrinter(getDefaultPrinterName())
			let print = {}
			let jobs = print.jobs || [];
			log.info('jobs', jobs.length)
			for (let i = 0;i < jobs.length;i ++) {
				let job= jobs[0];
				if (job.document === filename) {
					clearInterval(checkId)
					clearTimeout(timeoutId)
					log.info('job Id',job.id)
					resolve(job.id)
					break
				}
			} 
			// reject(new Error('notfound'))
		}, 100)

		let timeoutId = setTimeout(() => {
			clearInterval(checkId);
			log.info('get print job timeout')
			reject(new Error('timeout'))
		}, 15000)
	})
}
		
module.exports = gsprintFile;