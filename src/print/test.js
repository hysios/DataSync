const {printTemplate, getPrinters} = require('./index');
const {generateOrder} = require('../test/order_test');
const util = require('util');
let order = generateOrder()
let corpOpts = {
	corpname: '耒阳市方盈商贸有限责任公司', 
	corpaddress: '耒阳市文化路（环保局对面) 4331139'
}

order.totalAmount = order.items.reduce((s, it) => s+=it.quantity*it.price, 0)
console.log("installed printers:\n"+util.inspect(getPrinters(), {colors:true, depth:10}));
console.log("assign environment PRINTER a printer name, just like export PRINTER=EPSON_LQ_635K")
printTemplate('test', Object.assign({title: '耒阳市方盈商贸销售单', order: order}, corpOpts), {format: 'A4' /*, orientation: 'landscape'*/})
