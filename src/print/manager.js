const EventEmitter = require('events');
const {defaultPrinter, getJob} = require('./index');
const log = require('../log');

class PrinterManager extends EventEmitter {
	static push(jobId, options = {}) {
		this.instance.push(jobId, options)
	}

	static on(message, ...args) {
		this.instance.on(message, ...args)
	}

	constructor(options = {}) {
		super()
		this._queue = []
		this.tick = 300
		this.debug = options['debug']
		this.interval = this.interval.bind(this);
		this.taskId = setInterval(this.interval, this.tick)
	}

	push(jobId, options = {}) {
		let printerName = options['printerName'] || defaultPrinter().name
		this._queue.push({
			jobId: jobId,
			printerName: printerName,
			job: getJob(printerName, jobId)
		})
	}

	interval() {
		let closes = [];
		for (let i = 0; i < this._queue.length; i++) {
			let jobTask = this._queue[i];
			let {printerName} = jobTask;
			let job = getJob(printerName, jobTask.jobId)

			if (jobTask.job && jobTask.job.status[0] !== job.status[0]) {
				this.emit('order:print:status', jobTask)
			}

			jobTask.job = job;

			if (this.debug) {
				log.info(job.status)
			}

			if (!!~job.status.indexOf('CANCELLED') || !!~job.status.indexOf('PRINTED')) {
				closes.push(i)
			}
		}

		while(closes.length > 0) {
			let idx = closes.shift()
			this._queue.splice(idx, 1)
		}
	}

	autoClose(jobtask) {

	}
}

PrinterManager.instance = new PrinterManager({debug: true});

module.exports = PrinterManager;