const fs = require('fs');
// const pdf = require('html-pdf');
const path = require('path')
const ejs = require('ejs')
const tmp = require('tmp')
const options = { format: 'Letter' };
const Promise = require('bluebird');
const { exec } = require('child_process');
const moment = require('moment');
const {BrowserWindow, app} = require('electron')
// const printer = require('electron-printer');
const config = require('../config')();
const {API_SERVER} = require('../constants');
const log = require('../log');
// const pdfium = require('pdfium');
const gsprintFile = require('./win');
const convertFactory = require('electron-html-to');
const conversion = convertFactory({
	converterPath: convertFactory.converters.PDF,
	allowLocalFilesAccess: true
});

// const imagemagick = require('imagemagick-native');
moment.locale('zh-cn');

Promise.promisifyAll(tmp);
const execAsync = Promise.promisify(exec);

function createToPDF2(html, filename, options = {}) {
	return new Promise((resolve, reject) => {
		// let pdffile = pdf.create(html, options)
		log.info('pdf', filename)
		conversion({ html: html }, (err, result) => {
		    if (err) return reject(err);
		    result.stream.pipe(fs.createWriteStream(filename));
		    result.stream.on('end', resolve)
		    result.stream.on('error', reject)
		    // conversion.kill(); // necessary if you use the electron-server strategy, see bellow for details
		});		
		// pdffile.toFile(filename, function(err, res) {
		// 	if (err) {
		// 		reject(err)
		// 		return
		// 	}
		// 	resolve(res)
		// })
	})
}

function createPrintWin(name, context, options = {}) {
	return new Promise((resolve, reject) => {
		let {show, keepWindow} = options;
		let win = new BrowserWindow({show})
		let raw = JSON.stringify({
			tplName: name,
			context: context
		});

		win.loadURL(`${API_SERVER}/printer`, {
			postData: [{
				type: 'rawData',
		    	bytes: Buffer.from(raw)
			}],
			extraHeaders: 'Content-Type: application/json'
		})

		win.webContents.on('did-finish-load', () => {
			resolve(win)
		})
	})
}

function createPDFFile(win, filename, options = {}) {
	let {printerName, marginsType} = options;
	let pdfOpt = {
		silent: true,
		printBackground: true,
		landscape: true,
		deviceName: printerName,
		marginsType
	}

	return new Promise((resolve, reject) => {

		win.webContents.printToPDF(pdfOpt, (err, data) => {
			if (err) {
				reject(err)
				return 
			}
			
			fs.writeFile(filename, data, (err) => {
				if (err) {
					reject(err)
					return
				}

				resolve()
			})
		})
	})
}

function printWinFile(win, options = {}) {
	let {printerName, marginsType, silent, landscape} = options;
	let pdfOpt = {
		silent: silent,
		printBackground: silent,
		landscape: landscape,
		deviceName: printerName,
		marginsType
	}

	return new Promise((resolve, reject) => {
		win.webContents.print(pdfOpt, (err, data) => {
			if (err) {
				reject(err)
				return 
			}
			resolve()
		})
	})
}


function createTmp(handle) {
	tmp.dir(function _tempDirCreated(err, dir, cleanupCallback) {
  		if (err) throw err;

		handle(dir)
			.then(() => {
				cleanupCallback()
			})
			.catch(err => {
				cleanupCallback()
			})
	})
}

function printTemplate(tplname, context, options) {
	return new Promise((resolve, reject) => {
		let printerName = options.printerName || process.env.PRINTER;

		createPrintWin(tplname, context, options)
			.then((win) => {
				return printWinFile(win, options)
			})
	})
}

function generateHTML(tplName, context) {
	let tplfile = path.join(__dirname, '../templates', tplName + '.ejs');
		log.info('tplfile', tplfile)

	let tpldata = fs.readFileSync(tplfile, 'utf8')
	let tpl = ejs.compile(tpldata, {filename: tplfile});
	return tpl(Object.assign(context || {}, defaultScope))
}

function printTemplate2(tplname, context, options) {
	return new Promise((resolve, reject) => {
		let appPath = __dirname;
		let tplfile = path.join(appPath, '../templates', tplname + '.ejs');
		log.info('tplfile', tplfile)
		let printerName = options.printerName || process.env.PRINTER;
		delete options['printerName'];		
		try {
			let tpldata = fs.readFileSync(tplfile, 'utf8')
			let tpl = ejs.compile(tpldata, {filename: tplfile});
			let html = tpl(Object.assign(context || {}, defaultScope))

			createTmp((dir) => {
				return new Promise((cleanResolve) => {
					let name = path.basename(tmp.tmpNameSync({postfix: '.pdf'}));
					let filename = path.join(dir, name)
					createToPDF(html, filename, options).then(res => {
						if (options.preview) {
							execAsync('open ' + res.filename)
						}
						return res.filename
					})
					.then((filename) => {
						return printAsync(filename, printerName)
					})
					.then(res => {
						log.info('print success', res)
						cleanResolve();
						resolve(res)
					})
					.catch( err => {
						log.error(err, err.stack)
						reject(err)
					})
				})
			})
		} catch (err) {
			log.error(err, err.stack)
			reject(err)
		}
	})	

}

function getPrinters () {
	let win = new BrowserWindow()
	let printers = win.webContents.getPrinters()
	win.close()
	return printers
}

function getDefaultPrinterName() {
	let printer = getPrinters().filter((printerInfo) => printerInfo.isDefault);
	return printer ? printer.name : '';
}

function defaultPrinter() {
	let {defaultPrinter} = config;
	let printers = getPrinters();
	let firstPrinter = printers.length > 0; printers[0]; null;

	let isAvailablePrinter = (name) => {
		return printers.find(print => print.name === name) !== undefined
	}

	if (isAvailablePrinter(defaultPrinter)) {
		return defaultPrinter
	}

	let printerName = getDefaultPrinterName();
	if (printerName) {
		return {name: printerName}
	}
	return null;
}

function printAsync(filename, printerName) {
	log.info('print async', filename, printerName)
	
	return new Promise((resolve, reject) => {

		if( process.platform !== 'win32') {
			reject(new Error('non support printer.printFile'))
			// printer.printFile({
			// 	filename: filename,
			// 	printer: printerName, // printer name, if missing then will print to default printer
			// 	success: resolve,
			// 	error: reject
			// });	
		} else {

			gsprintFile(filename, printerName)
				.then(resolve)
				.catch(reject)
			// let data = pdfium.render(options);
			// printer.printDirect({
			// 	data: data,
			// 	type: 'EMF',
			//     printer: printerName, // printer name, if missing then will print to default printer
			//     success: resolve,
			// 	error: reject
	 	//  	});	
		// 		// });	
		}
	})
}

function printFile(filename, printerName) {
	return new Promise((resolve, reject) => {
		reject(new Error('non support printer.printFile'))
		// printer.printFile({
		// 	filename: filename,
		// 	printer: printerName, // printer name, if missing then will print to default printer
		// 	success: resolve,
		// 	error: reject
		// })	
	})
}

function numberToChinese(n) {
  if (!/^(0|[1-9]\d*)(\.\d+)?$/.test(n))
    return "数据非法";
  var unit = "仟佰拾亿千佰拾万仟佰拾元角分", str = "";
  n += "00";
  var p = n.indexOf('.');
  if (p >= 0)
    n = n.substring(0, p) + n.substr(p + 1, 2);
  unit = unit.substr(unit.length - n.length);
  for (var i=0; i < n.length; i++)
    str += '零壹贰叁肆伍陆柒捌玖'.charAt(n.charAt(i)) + unit.charAt(i);
  return str.replace(/零(仟|佰|拾|角)/g, "零").replace(/(零)+/g, "零").replace(/零(万|亿|元)/g, "$1").replace(/(亿)万|壹(拾)/g, "$1$2").replace(/^元零?|零分/g, "").replace(/元$/g, "元整");
}


// const CHINESE_NUMBERS = {
// 	"0": "零",
// 	"1": "壹",
// 	"2": "贰",
// 	"3": "叁",
// 	"4": "肆",
// 	"5": "伍",
// 	"6": "陆",
// 	"7": "柒",
// 	"8": "捌",
// 	"9": "玖"
// }

// const CHINESE_UNITS = {
// 	"零": 0,
// 	"拾": 10,
// 	"佰": 100,
// 	"仟": 1000,
// 	"萬": 10000,
// 	"拾萬": 100000,
// 	"佰萬": 1000000,
// 	"仟萬": 10000000,
// 	"亿":   100000000,
// 	"拾亿": 1000000000,
// 	"佰亿": 10000000000,
// 	"仟亿": 100000000000
// }

const defaultScope = {
	stylesheet: function(name) {
		let cssfile = path.join(__dirname, '../templates', name + '.css')
		let stylesheet = fs.readFileSync(cssfile, 'utf8')
		return `<style>
			${stylesheet}
		</style>`
	},
	chCurrency: function(num) {
		// let ns = parseFloat(''+ num).toFixed(2)
		return numberToChinese(num)
	},
	currency: function(num) {
		// return parseFloat(''+ num).toFixed(2)
		return parseFloat(''+ num)
	},
	toCurrency: function(num) {
		return parseFloat(''+ num).toFixed(2)
	},
	date: function(d) {
		return moment(d).format("YYYY MMM Do")
	}
}

// const {getJob, getSupportedPrintFormats} = printer;

module.exports = {
	printTemplate,
	getPrinters,
	generateHTML,
	defaultPrinter,
// 	getJob,
// 	getSupportedPrintFormats
}