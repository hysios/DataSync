const {generateUUID} = require('../utils');
const Sequelize = require('sequelize');
const config = require('../config')();
const printf = require('printf');
const log = require('../log');

const {database} = config;


const sequelize = new Sequelize(global.DBNAME || database.dbname, database.username, database.password, {
	host: database.host,
	dialect: database.dialect,
	databaseVersion: '10.0.0',
	pool: {
	    max: 5,
	    min: 0,
	    idle: 0
	},
	// retry: {
	// 	max: 3
	// },
	dialectOptions: {
		connectTimeout: 2 * 60 * 1000,
		requestTimeout: 2 * 60 * 1000,
		packetSize: 256 * 1024 
	},
	logging: log.info
});

// class Sequelize.BOOLEAN extends Sequelize.BOOLEAN {

// 	_sanitize(value) {
// 	  if (value !== null && value !== undefined) {
// 	    if (Buffer.isBuffer(value) && value.length === 1) {
// 	      // Bit fields are returned as buffers
// 	      value = value[0];
// 	    }

// 	    if (_.isString(value)) {
// 	      // Only take action on valid Sequelize.BOOLEAN strings.
// 	      value = value === 'T' || value === 'true' ? true : value === 'F' || value === 'false' ? false : value;

// 	    } else if (_.isNumber(value)) {
// 	      // Only take action on valid Sequelize.BOOLEAN integers.
// 	      value = value === 1 ? true : value === 0 ? false : value;
// 	    }
// 	  }

// 	  return value;		
// 	}

// 	stringify(value, options) {
// 		console.log('stringify', value)
// 		return value ? 'T': 'F';
// 	}
// }

// Sequelize.BOOLEAN.prototype.key = Sequelize.BOOLEAN.key = 'BOOLEAN';


const codeTpl = /XK-000-\d+-\d+-\d+-(\d+)/;

function lastID(table) {
	return new Promise((resolve) => {
		let sql = `select top 1 keyvalue from keyvaluetable where tablename = '${table}'`;
		sequelize.query(sql, {type: sequelize.QueryTypes.SELECT, logging: null}).then(result => {
			let keyvalue = result[0];
			resolve(keyvalue ? keyvalue.keyvalue + 1: 1)
		})
	})	
}

function tableOption(name) {
	return {
		timestamps: false,
		paranoid: false,

		// don't use camelcase for automatically added attributes but underscore style
		// so updatedAt will be updated_at
		underscored: false,
		freezeTableName: true,
		tableName: name
	}
}

const KeyTable = sequelize.define('keyvaluetable', {
	tablename: {
		type:Sequelize.STRING(64),
		allowNull: false,
	},
	keyvalue: Sequelize.INTEGER
}, tableOption('keyvaluetable'))

KeyTable.increment = function(table, count = 1) {
	let sql = `
		BEGIN TRAN
		IF EXISTS (SELECT * FROM keyvaluetable WITH (updlock,serializable) WHERE tablename = '${table}')
		BEGIN
		   UPDATE keyvaluetable SET keyvalue = keyvalue + ${count}
		   WHERE tablename = '${table}';
		END
		ELSE
		BEGIN
		   INSERT INTO keyvaluetable (tablename, keyvalue)
		   VALUES ('${table}', ${count})
		END
		COMMIT TRAN
	`
	return sequelize.query(sql, {type: sequelize.QueryTypes.UPDATE, logging: null})
 
	// sequelize.query(sql, {type: sequelize.QueryTypes.SELECT}).then(result => {
	// 	let keyvalue = result[0];
	// })
	// return KeyTable.find({where: {table: table}}).then(key => {
	// 	key.keyvalue += count
	// 	return key.save
	// })
}

KeyTable.setKey = function(table, lastID) {
	let sql = `
		BEGIN TRAN
		IF EXISTS (SELECT * FROM keyvaluetable WITH (updlock,serializable) WHERE tablename = '${table}')
		BEGIN
		   UPDATE keyvaluetable SET keyvalue = ${lastID}
		   WHERE tablename = '${table}';
		END
		ELSE
		BEGIN
		   INSERT INTO keyvaluetable (tablename, keyvalue)
		   VALUES ('${table}', ${lastID})
		END
		COMMIT TRAN
	`

	return sequelize.query(sql, {type: sequelize.QueryTypes.UPDATE, logging: null})
}

const Operator = sequelize.define('operator',  {
	opid: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
	opcode: Sequelize.STRING(3),
	opname: Sequelize.STRING(64),
	pass: Sequelize.STRING(32),
	privilege: Sequelize.BLOB(16),
	opgroup: Sequelize.BLOB(16),
	descript: Sequelize.STRING(64),
	mustchangepass: Sequelize.INTEGER,
	isuse: Sequelize.INTEGER,
	canchangepass: Sequelize.INTEGER,
	upflag: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	sysmanager: Sequelize.INTEGER,
	shopid: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	maxbuyamt: {
		type: Sequelize.DOUBLE,
		allowNull: false
	},
	maxsaleamt: {
		type: Sequelize.DOUBLE,
		allowNull: false
	},
	empid: Sequelize.INTEGER,
	win8: Sequelize.BLOB(16),
	isservice: {
		type: Sequelize.INTEGER, allowNull: false
	},
	isclient: {
		type: Sequelize.INTEGER, allowNull: false
	} 

}, tableOption('operator'))


// clientkind,isdouble,code,shortname,name,areaid,lev,suramt,balance,credit,initbillid,contator,mobilephone,fax,address,zip,bank,bankid,taxid,pager,url,email,memo,phone,pyassist,empid,legalrep,tradetypeid,departmentid,shiptype,shipto,traderid,opid,shopid,closed,upflag,msn,purmsnmodelid,pordermsn,porderemail,pordermodel,valuedrcvdmsn,valuedrcvdeamail,valuedrcvdmodel,recievedmsn,recievedemail,recievedmodel,paymsn,payemail,paymodel,preturnmsn,preturnemail,preturnmodel,salemsnmodelid,sordermsn,sorderemail,sordermodel,salemsn,saleemail,salemodel,deputemsn,deputeemail,deputemodel,chargemsn,chargeemail,chargemodel,returnmsn,returnemail,returnmodel,fixedcheck,checkperiod,sendday,lastcheck,channelid,tradertype2id,vipid,phoneapply,isexpress,expressid
const Client = sequelize.define('client', {
  clientid: {
	type: Sequelize.INTEGER,
	primaryKey: true
  },
  clientkind: {
  	type: Sequelize.INTEGER,
  	allowNull: false
  },
  isdouble: {
  	type: Sequelize.BOOLEAN,
  	allowNull: false,
  	set(val) {
  		this.setDataValue('isdouble', val ? 'T': 'F')
  	}
  },
  code: {
  	type: Sequelize.STRING,
  	allowNull: false
  },
  shortname: {
	type: Sequelize.STRING,
	allowNull: false
  },
  name: {
  	type: Sequelize.STRING,
  },
  areaid: {
  	type: Sequelize.INTEGER,
  },
  lev: Sequelize.INTEGER,
  suramt: Sequelize.DOUBLE,
  balance: Sequelize.DOUBLE,
  credit: Sequelize.DOUBLE,
  initbillid: Sequelize.INTEGER,
  contator: Sequelize.STRING(32),
  mobilephone: Sequelize.STRING(32),
  fax: Sequelize.STRING(32),
  address: Sequelize.STRING(64),
  zip: Sequelize.STRING(32),
  bank: Sequelize.STRING(64),
  bankid: Sequelize.STRING(64),
  taxid: Sequelize.STRING(32),
  pager: Sequelize.STRING(32),
  url: Sequelize.STRING(64),
  email: Sequelize.STRING(64),
  memo: Sequelize.STRING(64),
  phone: Sequelize.STRING(32),
  pyassist: Sequelize.STRING(32),
  empid: Sequelize.INTEGER,
  legalrep: Sequelize.STRING(32),
  tradetypeid: Sequelize.INTEGER,
  departmentid: Sequelize.INTEGER,
  shiptype: Sequelize.INTEGER,
  shipto: Sequelize.STRING(32),
  traderid: Sequelize.INTEGER,
  opid: Sequelize.INTEGER,
  shopid: Sequelize.INTEGER,
  closed: {
  	type: Sequelize.BOOLEAN,
  	set(val) {
  		this.setDataValue('closed', val ? 'T': 'F')
  	}
  },
  upflag: Sequelize.INTEGER,
  msn: Sequelize.STRING(64),
  purmsnmodelid: Sequelize.INTEGER,
  pordermsn: Sequelize.BOOLEAN,
  porderemail: Sequelize.BOOLEAN,
  pordermodel: Sequelize.STRING(128),
  valuedrcvdmsn: Sequelize.BOOLEAN,
  valuedrcvdeamail: Sequelize.BOOLEAN,
  valuedrcvdmodel: Sequelize.STRING(128),
  recievedmsn: Sequelize.BOOLEAN,
  recievedemail: Sequelize.BOOLEAN,
  recievedmodel: Sequelize.STRING(128),
  paymsn: Sequelize.BOOLEAN,
  payemail: Sequelize.BOOLEAN,
  paymodel: Sequelize.STRING(128),
  preturnmsn: Sequelize.BOOLEAN,
  preturnemail: Sequelize.BOOLEAN,
  preturnmodel: Sequelize.STRING(128),
  salemsnmodelid: Sequelize.INTEGER,
  sordermsn: Sequelize.BOOLEAN,
  sorderemail: Sequelize.BOOLEAN,
  sordermodel: Sequelize.STRING(128),
  salemsn: Sequelize.BOOLEAN,
  saleemail: Sequelize.BOOLEAN,
  salemodel: Sequelize.STRING(128),
  deputemsn: Sequelize.BOOLEAN,
  deputeemail: Sequelize.BOOLEAN,
  deputemodel: Sequelize.STRING(128),
  chargemsn: Sequelize.BOOLEAN,
  chargeemail: Sequelize.BOOLEAN,
  chargemodel: Sequelize.STRING(128),
  returnmsn: Sequelize.BOOLEAN,
  returnemail: Sequelize.BOOLEAN,
  returnmodel: Sequelize.STRING(128),
  fixedcheck: Sequelize.BOOLEAN,
  checkperiod: Sequelize.INTEGER,
  sendday: Sequelize.INTEGER(4),
  lastcheck: Sequelize.INTEGER,
  channelid: Sequelize.INTEGER,
  tradertype2id: Sequelize.INTEGER,
  vipid: Sequelize.INTEGER,
  phoneapply: Sequelize.BOOLEAN,
  isexpress: Sequelize.BOOLEAN,
  expressid: Sequelize.INTEGER
}, tableOption('client'));

Client.lastCode = function() {
	return new Promise((resolve) => {
		Client.find({order: 'clientid desc'}).then(client => {
			if (client) {
				resolve(generateUUID())
			} else {
				resolve("0001")
			}
		})
	})
}

Client.lastID = function() {
	return lastID('client')
}

const Area = sequelize.define('area', {
	areaid: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
	name: {
		type: Sequelize.STRING(64),
		allowNull: false
	},
	lcode: {
		type: Sequelize.STRING(10),
		allowNull: false
	},
	parentid: Sequelize.INTEGER,
	areacode: Sequelize.STRING(32),
	upflag: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	shopid: {
		type: Sequelize.INTEGER,
		allowNull: false
	}
}, tableOption('area'))

const Store = sequelize.define('store', {
	storeid: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
	name: {
		type: Sequelize.STRING(64),
		allowNull: false
	},
	location: Sequelize.STRING(64),
	memo: Sequelize.STRING(64),
	upflag: Sequelize.INTEGER,
	operators: Sequelize.STRING(2000),
	shopid: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	closed: {
		type: Sequelize.BOOLEAN,
		allowNull: false
	}
}, tableOption('store'))

const Shop = sequelize.define('shop', {
	shopid: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
	name: {
		type: Sequelize.STRING(64),
		allowNull: false
	},
	code: {
		type: Sequelize.STRING(32),
		allowNull: false
	},
	leaglrep: Sequelize.STRING(64),
	bank: Sequelize.STRING(64),
	bankaccno: Sequelize.STRING(64),
	taxno: Sequelize.STRING(32),
	address: Sequelize.STRING(64),
	phone: Sequelize.STRING(32),
	upflag: {
		type: Sequelize.INTEGER,
		allowNull: false
	}
}, tableOption('shop'))
// 销售开单
const Invoice = sequelize.define('invoice', {
	invoiceid: {
		type: Sequelize.INTEGER,
		primaryKey: true,
	},
	code: Sequelize.STRING(32),
	billdate: Sequelize.DATEONLY,
	storeid: Sequelize.INTEGER,
	clientid: Sequelize.INTEGER,
	departmentid: Sequelize.INTEGER,
	exemanid: Sequelize.INTEGER,
	rptid: Sequelize.INTEGER,
	disc: Sequelize.INTEGER,
	taxrate: Sequelize.DOUBLE,
	linkmanid: Sequelize.INTEGER,
	opid: Sequelize.INTEGER,
	memo: Sequelize.STRING(255),
	termdays: Sequelize.INTEGER,
	totalamt: Sequelize.DOUBLE,
	totalrcvd: Sequelize.DOUBLE,
	billtypeid: Sequelize.INTEGER,
	shipto: Sequelize.STRING(64),
	checkno: Sequelize.STRING(32),
	checkamt: Sequelize.DOUBLE,
	templateid: Sequelize.INTEGER,
	shopid: Sequelize.INTEGER,
	userdef1: Sequelize.STRING(64),
	userdef2: Sequelize.STRING(64),
	userdef3: Sequelize.STRING(64),
	userdef4: Sequelize.STRING(64),
	userdef5: Sequelize.STRING(64),
	flag: Sequelize.INTEGER,
	referbillid: Sequelize.INTEGER,
	refercount: Sequelize.INTEGER,
	updatetime: Sequelize.DATE,
	billstatus: Sequelize.INTEGER,
	printed: Sequelize.BOOLEAN,
	credid: Sequelize.STRING(32),
	phone: Sequelize.STRING(32),
	fax: Sequelize.STRING(32),
	contator: Sequelize.STRING(32),
	bsrptid: Sequelize.INTEGER,
	sendemail: Sequelize.BOOLEAN,
	printcount: Sequelize.INTEGER,
	subscore: Sequelize.INTEGER,
	score: Sequelize.INTEGER,
	projectid: Sequelize.INTEGER,
	filecount: Sequelize.INTEGER
}, tableOption('invoice'))

Invoice.belongsTo(Client, {foreignKey: 	'clientid', sourceKey: 'clientid' })
Invoice.belongsTo(Store, {foreignKey: 'storeid', sourceKey: 'storeid'})

Invoice.lastCode = function(date = new Date()) {
  let orderBy =  [['code', 'desc'], ['billdate', 'desc']];
  let day = date.toISOString().slice(0, 10);
  let where = {billdate: day}
  
  return new Promise(resolve => {
    Invoice.find({order: orderBy, where}).then(invoice => {
      let initCode = `XK-000-${day}-0001`;
      if (!invoice) {
        resolve(initCode)
        return 
      }
      let matchs = codeTpl.exec(invoice.code || '');
      if (matchs.length > 1) {
        let initCode = `XK-000-${day}-0001`;
        let idx = parseInt(matchs[1])
        resolve(printf('XK-000-%s-%04d', day, ++idx))  
      } else {
        resolve(initCode)
      }
    })
  })
}

Invoice.lastID = function() {
	// return new Promise(resolve => {
	//   	Invoice.find({order: 'invoiceid desc'}).then(invoice => {
	//   		if (!invoice) {
	//   			return resolve(1);
	//   		}

	//   		resolve(++invoice.invoiceid);
	//   	})

	// })
  return lastID('invoice');
}

const Received = sequelize.define('received', {
	recievedid: {
		type: Sequelize.INTEGER,
		primaryKey: true,
	},
	code: {
		type: Sequelize.STRING(32),
		allowNull: false
	},
	billdate: {
		type: Sequelize.DATEONLY,
		allowNull: false
	},
	storeid: Sequelize.INTEGER,
	shopid: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	clientid: {
		type: Sequelize.INTEGER,
		allowNull: false,
	},
	departmentid: Sequelize.INTEGER,
	exemanid: Sequelize.INTEGER,
	disc: {
		type: Sequelize.INTEGER,
		allowNull: false,
	},
	totalamt: {
		type: Sequelize.DOUBLE,
		allowNull: false
	},
	totalpaid: {
		type: Sequelize.DOUBLE,
		allowNull: false
	},
	billtypeid: Sequelize.INTEGER,
	termdays: Sequelize.INTEGER,
	checkno: Sequelize.STRING(32),
	checkamt: {
		type: Sequelize.DOUBLE,
		allowNull: false
	},
	memo: Sequelize.STRING(255),
	userdef1: Sequelize.STRING(64),

	userdef2: Sequelize.STRING(64),
	userdef3: Sequelize.STRING(64),
	userdef4: Sequelize.STRING(64),
	userdef5: Sequelize.STRING(64),
	templateid: Sequelize.INTEGER,
	rptid: Sequelize.INTEGER,
	flag: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	referbillid: Sequelize.INTEGER,
	refercount: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	updatetime: Sequelize.DATE,
	printed: Sequelize.BOOLEAN,
	credid: Sequelize.STRING(12),
	phone: Sequelize.STRING(32),
	fax: Sequelize.STRING(32),
	contator: Sequelize.STRING(32),
	linkmanid: Sequelize.INTEGER,
	bsrptid: Sequelize.INTEGER,
	sendemail: Sequelize.BOOLEAN,
	printcount: Sequelize.INTEGER,
	projectid: Sequelize.INTEGER,
	filecount: {
		type: Sequelize.INTEGER,
		allowNull: false
	}
}, tableOption('recieved'));
Received.belongsTo(Client, { foreignKey: 'clientid', sourceKey: 'clientid'})
Received.belongsTo(Store, { foreignKey: 'storeid', sourceKey: 'storeid'})

const ReceivedDetail = sequelize.define('receiveddetail', {
	recievedid: {
		type: Sequelize.INTEGER,
		primaryKey: true,
	},
	itemno: {
		type: Sequelize.INTEGER,
		primaryKey: true,
	},
	unitid: {
		type: Sequelize.INTEGER,
		allowNull: false,
	},
	goodsid: {
		type: Sequelize.INTEGER,
		allowNull: false,
	},
	price: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	quantity: {
		type: Sequelize.INTEGER,
		allowNull: false,
	},
	unitprice: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	unitqty:  {
		type: Sequelize.INTEGER,
		allowNull: false,
	},
	taxrate:  {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	goodsamt:  {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	taxamt:  {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	amount:  {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	disc:  {
		type: Sequelize.INTEGER,
		allowNull: false,
	},
	batchrefid: Sequelize.INTEGER,
	batchcode: Sequelize.STRING(32),
	produceddate: Sequelize.DATEONLY,
	validdate: Sequelize.DATEONLY,
	memo: Sequelize.STRING(64),
	userdef1: Sequelize.STRING(64),
	userdef2: Sequelize.STRING(64),
	userdef3: Sequelize.STRING(64),
	returnqty: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	returnamt: {
		type: Sequelize.DOUBLE,
		allowNull: false
	},
	refertype: Sequelize.INTEGER,
	referbillid: Sequelize.INTEGER,
	referitemno: Sequelize.INTEGER,
	refercount: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	ispresent: {
		type: Sequelize.BOOLEAN,
		allowNull: false
	},
	purappamt: {
		type: Sequelize.DOUBLE,
		allowNull: false
	},
	nprice: Sequelize.DOUBLE,
	checkrate: Sequelize.INTEGER,
	checkdate: Sequelize.DATEONLY,
	checkempid: Sequelize.INTEGER,
	checkresult: Sequelize.BOOLEAN,
	goodsaddress: Sequelize.STRING(32),
	barcodeid: Sequelize.INTEGER,
	pricetype: Sequelize.STRING(32)	
}, tableOption('rcvddetail'));
Received.hasMany(ReceivedDetail, {foreignKey: 'recievedid', sourceKey: 'recievedid', as: 'receivedDetails'})

const Employ = sequelize.define('employ', {
	empid: {
		type: Sequelize.INTEGER,
		primaryKey: true,
	},
	department: Sequelize.INTEGER,
	emptypeid: Sequelize.INTEGER,
	empcode: Sequelize.STRING(32),
	name: Sequelize.STRING(64),
	duty: Sequelize.STRING(32),
	postcode: Sequelize.STRING(32),
	forefather: Sequelize.STRING(32),
	postdate: Sequelize.DATEONLY,
	sign: Sequelize.BOOLEAN,
	pay: Sequelize.DOUBLE,
	countmeth: Sequelize.BOOLEAN,
	memory: Sequelize.STRING(255),
	isoperate: Sequelize.BOOLEAN,
	sex: Sequelize.BOOLEAN,
	birthday: Sequelize.DATE,
	idcard: Sequelize.STRING(32),
	address: Sequelize.STRING(64),
	email: Sequelize.STRING(64),
	culture: Sequelize.STRING(64),
	bp: Sequelize.STRING(32),
	telephone: Sequelize.STRING(32),
	homephone: Sequelize.STRING(32),
	upflag: Sequelize.INTEGER,
	bankno: Sequelize.STRING(32),
	incometax: Sequelize.BOOLEAN,
	mobilephone: Sequelize.STRING(32),
	shopid: Sequelize.INTEGER,
	password: Sequelize.STRING(32),
	msn: Sequelize.STRING(64)
}, tableOption('employ'));

Employ.lastID = function() {
	// return new Promise((resolve) => {
	// 	Employ.find({order: 'empid desc'}).then(employ => {
	// 		if (employ) {
	// 			resolve(++employ.empid)
	// 		} else {
	// 			resolve(1)
	// 		}
	// 	})
	// })
	return lastID('employ')
}

Employ.lastCode = function() {
	return new Promise((resolve) => {
		Employ.find({order: 'empid desc'}).then(employ => {
			if (employ) {
				resolve(generateUUID())
			} else {
				resolve("0001")
			}
		})
	})
}

Invoice.belongsTo(Employ, {foreignKey: 'exemanid', sourceKey: 'empid'})

const Good = sequelize.define('good', {
	goodsid: {
		type: Sequelize.INTEGER,
		primaryKey: true,
	},
	code: Sequelize.STRING(32),
	name: Sequelize.STRING(64),
	shortname: Sequelize.STRING(64),
	specs: Sequelize.STRING(64),
	unit: Sequelize.STRING(32),
	inf_costingtypeid: Sequelize.INTEGER,
	gcmid: Sequelize.INTEGER,
	kind: Sequelize.INTEGER,
	goodstypeid: Sequelize.INTEGER,
	typeflag: Sequelize.INTEGER,
	topqty: Sequelize.INTEGER,
	downqty: Sequelize.INTEGER,
	onhand: Sequelize.INTEGER,
	aprice: Sequelize.DOUBLE,
	lowprice: Sequelize.DOUBLE,
	highprice: Sequelize.DOUBLE,
	validdates: Sequelize.INTEGER,
	cancmb: Sequelize.BOOLEAN,
	clientid: Sequelize.INTEGER,
	leadtime: Sequelize.INTEGER,
	minpurcaseqty: Sequelize.INTEGER,
	maxpurcaseqty: Sequelize.INTEGER,
	orderaddqty: Sequelize.INTEGER,
	pyassist: Sequelize.STRING(32),
	comminfoid: Sequelize.INTEGER,
	affixinfoid: Sequelize.INTEGER,
	shopid: Sequelize.INTEGER,
	batchctrl: Sequelize.BOOLEAN,
	closed: Sequelize.BOOLEAN,
	status: Sequelize.BOOLEAN,
	upflag: Sequelize.INTEGER,
	unitname1: Sequelize.STRING(32),
	unitrate1: Sequelize.INTEGER,
	unitname2: Sequelize.STRING(32),
	unitrate2: Sequelize.INTEGER,
	unitname3: Sequelize.STRING(32),
	unitrate3: Sequelize.INTEGER,	
	unitname4: Sequelize.STRING(32),
	unitrate4: Sequelize.INTEGER,	
	unitname5: Sequelize.STRING(32),
	unitrate5: Sequelize.INTEGER,
	goodsuserdef1: Sequelize.STRING(64),
	goodsuserdef2: Sequelize.STRING(64),
	goodsuserdef3: Sequelize.STRING(64),
	goodsuserdef4: Sequelize.STRING(64),
	goodsuserdef5: Sequelize.STRING(64),
	goodsuserdef6: Sequelize.STRING(64),
	goodsuserdef7: Sequelize.STRING(64),
	goodsuserdef8: Sequelize.STRING(64),
	goodsuserdef9: Sequelize.STRING(64),
	goodsuserdef10: Sequelize.STRING(64),
	manufactuerid: Sequelize.INTEGER,
	goodsaddress: Sequelize.STRING(32),
	sizegroupid: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	colorgroupid: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	bomqty: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	scorerate: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	subducescore: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	phoneapply: {
		type: Sequelize.BOOLEAN,
		allowNull: false
	},
	dsgoods: {
		type: Sequelize.BOOLEAN,
		allowNull: false
	},
	dscorpid: Sequelize.INTEGER,
	dsgoodscode: Sequelize.STRING(32),
	saletaxrate: Sequelize.INTEGER
}, tableOption('goods'));

const GoodUnit = sequelize.define('goodunit', {
	unitid: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
	goodsid: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	unitname: Sequelize.STRING(32),
	barcode: Sequelize.STRING(64),
	unittype: Sequelize.INTEGER,
	rate: Sequelize.DOUBLE,
	pprice: Sequelize.DOUBLE,
	sprice: Sequelize.DOUBLE,
	lprice1: {
		type: Sequelize.DOUBLE,
		allowNull: false
	},
	lprice2: {
		type: Sequelize.DOUBLE,
		allowNull: false
	},
	lprice3: {
		type: Sequelize.DOUBLE,
		allowNull: false
	},	
	lprice4: {
		type: Sequelize.DOUBLE,
		allowNull: false
	},	
	lprice5: {
		type: Sequelize.DOUBLE,
		allowNull: false
	},
	weightcode: {
		type: Sequelize.STRING(5)
	},
	vipprice: {
		type: Sequelize.DOUBLE,
		allowNull: false
	},
	upflag: {
		type: Sequelize.INTEGER
	},
	maxpprice: {
		type: Sequelize.DOUBLE,
		allowNull: false
	},
	minsprice: {
		type: Sequelize.DOUBLE,
		allowNull: false
	}
}, tableOption('goodsunit'))

const GoodType = sequelize.define('goodtype', {
	goodstypeid: {
		type: Sequelize.INTEGER,
		primaryKey: true		
	},
	lcode: {
		type: Sequelize.STRING(10),
		allowNull: false
	},
	name: {
		type: Sequelize.STRING(64),
		allowNull: false
	},
	description: Sequelize.STRING(64),
	upflag: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	parentid: Sequelize.INTEGER,
	shopid: {
		type: Sequelize.INTEGER,
		allowNull: false
	}
}, tableOption('goodstype'))

Good.lastID = function() {
	// return new Promise((resolve) => {
	// 	Good.find({order: 'goodsid desc'}).then(good => {
	// 		if (good) {
	// 			resolve(++good.goodsid)
	// 		} else {
	// 			resolve(1)
	// 		}
	// 	})
	// })
	return lastID('goods')
}

GoodUnit.lastID = function() {
	// return new Promise((resolve) => {
	// 	GoodUnit.find({order: 'unitid desc'}).then(goodunit => {
	// 		if (goodunit) {
	// 			resolve(++goodunit.unitid)
	// 		} else {
	// 			resolve(1)
	// 		}
	// 	})
	// })
	return lastID('goodsunit')
}

GoodType.lastID = function() {
	// return new Promise((resolve) => {
	// 	GoodType.find({order: 'goodstypeid desc'}).then(goodtype => {
	// 		if (goodtype) {
	// 			resolve(++goodtype.goodstypeid)
	// 		} else {
	// 			resolve(1)
	// 		}
	// 	})
	// })
	return lastID('goodstype')
}

Good.hasMany(GoodUnit, {foreignKey: 'goodsid', sourceKey: 'goodsid', as: 'goodUnits'})
Good.belongsTo(GoodType, {foreignKey: 'goodstypeid', sourceKey: 'goodstypeid', as: 'goodType'})
GoodUnit.belongsTo(Good, {foreignKey: 'goodsid', targetKey: 'goodsid', as: 'good'})

const SaleDetail = sequelize.define('saledetail', {
	invoiceid: {
		type: Sequelize.INTEGER,
		primaryKey: true,
	},
	itemno: {
		type: Sequelize.INTEGER,
		primaryKey: true,
	},
	goodsid: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	unitid: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	aprice: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	disc: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	price: {
		type: Sequelize.DOUBLE,
		allowNull: false
	},
	quantity: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	unitprice: {
		type: Sequelize.DOUBLE,
		allowNull: false
	},
	unitqty:  {
		type: Sequelize.INTEGER,
		allowNull: false,
	},	
	batchrefid: Sequelize.INTEGER,
	batchcode: Sequelize.STRING(64),
	produceddate: Sequelize.DATE,
	validdate: Sequelize.DATE,
	taxrate: Sequelize.DOUBLE,
	amount: Sequelize.DOUBLE,
	userdef1: Sequelize.STRING(64),
	userdef2: Sequelize.STRING(64),
	userdef3: Sequelize.STRING(64),	
	memo: Sequelize.STRING(64),
	refertype: Sequelize.INTEGER,
	referbillid: Sequelize.INTEGER,
	referitemno: Sequelize.INTEGER,
	ispresent: {
		type: Sequelize.BOOLEAN,
		allowNull: false
	},
	returnqty: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	returnamt: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	refercount: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	nprice: Sequelize.DOUBLE,
	goodsaddress: Sequelize.STRING(32),
	scorerate: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	barcodeid: Sequelize.INTEGER,
	pricetype: Sequelize.STRING(32)
}, tableOption('saledetail'))
Invoice.hasMany(SaleDetail, { foreignKey: 'invoiceid', sourceKey: 'invoiceid', as: 'saleDetails' })

SaleDetail.belongsTo(Invoice, { foreignKey: 'invoiceid', sourceKey: 'invoiceid' })
SaleDetail.belongsTo(Good, { foreignKey: 'goodsid', sourceKey: 'goodsid' })
SaleDetail.belongsTo(GoodUnit, { foreignKey: 'unitid', sourceKey: 'unitid' })


ReceivedDetail.belongsTo(Good, { foreignKey: 'goodsid', sourceKey: 'goodsid' })
ReceivedDetail.belongsTo(GoodUnit, { foreignKey: 'unitid', sourceKey: 'unitid' })

const Inout = sequelize.define('inv_inout', {
	storeid: Sequelize.INTEGER,
	store_name: Sequelize.STRING(64),
	billname: Sequelize.STRING(64),
	billdate: Sequelize.DATEONLY,
	billid: Sequelize.INTEGER,
	billno: Sequelize.STRING(32),
	goodsid: Sequelize.INTEGER,
	goods_code: Sequelize.STRING(32),
	goods_name: Sequelize.STRING(64),
	batchcode: Sequelize.STRING(32),
	produceddate: Sequelize.DATEONLY,
	validdate: Sequelize.DATEONLY,
	base_unitname: Sequelize.STRING(32),
	base_inqty: Sequelize.INTEGER,
	inamt: Sequelize.DOUBLE,
	base_outqty: Sequelize.INTEGER,
	base_outprice: Sequelize.DOUBLE,
	outamt: Sequelize.DOUBLE,
	bill_unitname: Sequelize.STRING(32),
	bill_inqty: Sequelize.DECIMAL(17),
	bill_inprice: Sequelize.DECIMAL(17,2),
	bill_outqty: Sequelize.DECIMAL(17),
	bill_outprice: Sequelize.DECIMAL(17,2),
	goods_typename: Sequelize.STRING(64),
	goods_gcmname: Sequelize.STRING(64),
	costing_name: Sequelize.STRING(64)
}, tableOption('inv_inout'))

Inout.removeAttribute("id");

const DetailBillFlow = sequelize.define('detailbillflow', {
	// id: {
	// 	type: Sequelize.INTEGER,
	// 	primaryKey: true
	// },
	billdate: {
		type: Sequelize.DATEONLY,
		allowNull: false,
	},
	billtype: {
		type: Sequelize.INTEGER,
		allowNull: false,
	},
	billid: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	itemno: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	billcode: Sequelize.STRING(32),
	storeid: Sequelize.INTEGER,
	goodsid: {
		type: Sequelize.INTEGER,
		allowNull: false,
	},
	unitid: {
		type: Sequelize.INTEGER,
		allowNull: false,
	},
	unitprice: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	unitqty: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	inqty: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	inprice: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	inamt: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	outqty: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	outprice: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	outamt: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	eqty: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	eprice: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	eamount: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	difamount: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	batchcode: Sequelize.STRING,
	produceddate: Sequelize.DATEONLY,
	validdate: Sequelize.DATEONLY,
	batchrefid: Sequelize.INTEGER,
	refqty: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	refamt: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	refcount: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	},
	caculateflag: {
		type: Sequelize.BOOLEAN,
		allowNull: false,
	  	set(val) {
  			this.setDataValue('caculateflag', val ? 'T': 'F')
  		}
	},
	needupdatebill: {
		type: Sequelize.BOOLEAN,
		allowNull: false,
	  	set(val) {
  			this.setDataValue('needupdatebill', val ? 'T': 'F')
  		}		
	},
	prevcalc: Sequelize.INTEGER,
	locked: {
		type: Sequelize.BOOLEAN,
		allowNull: false,
	  	set(val) {
  			this.setDataValue('locked', val ? 'T': 'F')
  		}			
	},
	lockdate: Sequelize.DATEONLY,
	goodsaddress: Sequelize.STRING(32)
}, tableOption('detailbillflow'))

DetailBillFlow.lastID = function() {
	return new Promise((resolve, reject) => {
	  	DetailBillFlow.findOne({order: 'id desc'}).then(billflow => {
	  		if (!billflow) {
	  			return resolve(1);
	  		}

	  		resolve(++billflow.id);
	  	}).catch(reject)
	})	
}

const Onhand = sequelize.define('onhand', {
	storeid: {
		type: Sequelize.INTEGER,
		allowNull: false,
	},
	goodsid: {
		type: Sequelize.INTEGER,
		allowNull: false,
	},
	onhand: {
		type: Sequelize.DOUBLE,
		allowNull: false,
	}
}, tableOption('onhand'))

Onhand.removeAttribute('id')

const Message = sequelize.define('message', {
	messageId: {
		type: Sequelize.STRING(64),
		allowNull: false
	},
	type: Sequelize.STRING(32),
	command: Sequelize.STRING(32),
	msgPart: Sequelize.INTEGER,
	msgCount: Sequelize.INTEGER,
	body: Sequelize.TEXT,
	status: Sequelize.STRING(32),
	returnURL: Sequelize.STRING(1024)
}, tableOption('__####messages'));

function listDatabases(prefix = 'SD11831N')  {
	let re_dbname = new RegExp("^" + prefix)
	return sequelize.query("SELECT name FROM master.dbo.sysdatabases").then(databases => {
		return databases[0].filter(({name}) => re_dbname.test(name))
	})
}
// console.dir(Message)
module.exports = {
	Operator, 					// 操作员
	Client,							// 客户/供应商
	Invoice,						// 销售
	Received,						// 采购
	ReceivedDetail, 				// 采购明细
	Employ,							// 雇员/业务员
	Good, 							// 货品
	GoodType,
	GoodUnit,					  // 货品单位
	SaleDetail,         // 销售单明细
	DetailBillFlow,
	Store, 				// 仓库
	KeyTable,
	Inout,
	Message,
	Sequelize,
	Area,
	Onhand,
	sequelize,
	listDatabases
}