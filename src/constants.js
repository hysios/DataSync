module.exports = {
	API_SERVER: process.env.NODE_ENV === 'production'? 'http://127.0.0.1:3001' : 'http://127.0.0.1:3000',
	COMMAND_QUEUE: process.env.COMMAND_QUEUE || 'data-sync',
	ITEM_MAX_DISPLAY: 5,
	GIFT_MAX_DISPLAY: 2,
	EMPTY: ''
}